#include "WaveObject.h"

#include "MathsUtils.h"

WaveObject::WaveObject()
	: GameObject() {
	type_ = kWave;
}
WaveObject::WaveObject(std::string name)
	: WaveObject() {
	name_ = name;
}

WaveObject::~WaveObject() {
	mesh_ = nullptr;
}

WaveShader::WaveParamaters* WaveObject::WaveParams() {
	return &params_;
}

float WaveObject::NearClip() {
	return params_.nearClip;
}

float WaveObject::FarClip() {
	return params_.farClip;
}

float WaveObject::MinTess() {
	return params_.minTess;
}

float WaveObject::MaxTess() {
	return params_.maxTess;
}

float WaveObject::TessAmp() {
	return params_.tessAmp;
}

float WaveObject::Amplitude() {
	return params_.amplitude;
}

float WaveObject::Steepness() {
	return params_.steepness;
}

float WaveObject::WaveLength() {
	return params_.waveLength;
}

void WaveObject::SetNearClip(float near_clip) {
	params_.nearClip = near_clip;
}

void WaveObject::SetFarClip(float far_clip) {
	params_.farClip = far_clip;
}

void WaveObject::SetMinTess(float min_tess) {
	params_.minTess = min_tess;
}

void WaveObject::SetMaxTess(float max_tess) {
	params_.maxTess = max_tess;
}

void WaveObject::SetTessAmp(float tess_amp) {
	params_.tessAmp = tess_amp;
}

void WaveObject::SetAmplitude(float amplitude) {
	params_.amplitude = LittleLot::Clamp(amplitude, 0.001f, 0.5f);
}

void WaveObject::SetSteepness(float steepness) {
	params_.steepness = LittleLot::Clamp(steepness, 0.001f, 0.5f);
}

void WaveObject::SetWaveLength(float wave_length) {
	params_.waveLength = LittleLot::Clamp(wave_length, 0.001f, 0.5f);
}