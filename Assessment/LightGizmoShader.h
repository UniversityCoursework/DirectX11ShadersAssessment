#ifndef _GIZMO_SHADER_H
#define _GIZMO_SHADER_H

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Light Gizmo Shader.
/// Used to render light positions with diffuse colour.
/// </summary>
class LightGizmoShader : public BaseShader {
private:
	struct ColourBufferType {
		XMFLOAT4 colour;
	};

public:
	LightGizmoShader(ID3D11Device* device, HWND hwnd);
	~LightGizmoShader();
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, XMFLOAT4 light_colour);
	void Render(ID3D11DeviceContext* device_context, int index_count);
private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11Buffer* colour_buffer_;
};

#endif