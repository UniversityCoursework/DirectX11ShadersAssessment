#ifndef _NOISE_PROCESS_H
#define _NOISE_PROCESS_H

#include "PostProcess.h"

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

/// <summary>
/// The different types of noise calculations available for this Effect.
/// </summary>
enum NoiseProcessType {
	kNoise1,
	kNoise2,
	kNoise3
};

/// <summary>
/// Various examples of Noise Calculations on the GPU.
/// </summary>
class NoiseProcess : public PostProcess {
public:
	NoiseProcess();
	~NoiseProcess();
	/// <summary>
	/// Set which type of noise calculation to use.
	/// </summary>	
	void SetNoiseType(NoiseProcessType new_type);

	/// <summary>
	/// Sets the amount to amplify the distortion by.	
	/// </summary>	
	void SetNoiseLevel(float new_level);

	NoiseProcessType NoiseType();
	float NoiseLevel();

protected:
	NoiseProcessType noise_type_;
	float noise_level_;
};

#endif

