#include "ParticleContainer.h"

#include "MathsUtils.h"

ParticleContainer::ParticleContainer(ID3D11Device * device, ID3D11DeviceContext * device_context, WCHAR* texture_filename, ParticleSettings settings)
	:num_particles_(0),
	max_particles_(settings.max_particles) {
	settings_ = settings;
	for (int i = 0; i < max_particles_; i++) {
		particles_.push_back(new Particle());
	}
	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, device_context, texture_filename);
}


ParticleContainer::~ParticleContainer() {
	// Run parent deconstructor
	BaseMesh::~BaseMesh();
	for each (Particle* particle in particles_) {
		if (particle) {
			delete particle;
			particle = nullptr;
		}
	}
}

void ParticleContainer::Update(float dt) {
	// iterate over active particles.
	for (int particle = 0; particle < num_particles_; ++particle) {
		particles_[particle]->duration_remaining_ -= dt;
		if (particles_[particle]->duration_remaining_ < 0) {
			// As long as its not the last item, ie move to self
			if (num_particles_ > 1) {
				// Move the last alive item to this position.
				*particles_[particle] = *particles_[num_particles_ - 1];
			}
			// Now ignore the last particle, as it is now dead.
			num_particles_--;
		} else {
			UpdateParticle(dt, particle);
		}
	}
}

void ParticleContainer::SendData(ID3D11DeviceContext* device_context) {
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(ParticlePoint);
	offset = 0;
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device_context->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device_context->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case control patch for tessellation.
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
}

void ParticleContainer::BuildBuffers(ID3D11Device * device) {
	InitBuffers(device);
}

void ParticleContainer::AddParticle(XMFLOAT3 position) {
	if (num_particles_ < max_particles_) {
		particles_[num_particles_]->set_duration(settings_.max_duration);
		particles_[num_particles_]->set_size(settings_.max_start_size);
		particles_[num_particles_]->set_end_size(settings_.max_end_size);
		particles_[num_particles_]->set_position(XMVectorSet(position.x, position.y, position.z, 0.0f));
		particles_[num_particles_]->set_velocity(settings_.max_x_velocity, settings_.max_y_velocity, settings_.max_z_velocity);
		++num_particles_;
	}
}

int ParticleContainer::NumOfParticles() {
	return num_particles_;
}

int ParticleContainer::MaxNumOfParticles() {
	return max_particles_;
}

ParticleSettings * ParticleContainer::ContainerParticleSettings() {
	return &settings_;
}

void ParticleContainer::InitBuffers(ID3D11Device * device) {

	ParticlePoint* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	// Set the number of vertices in the vertex array.
	m_vertexCount = num_particles_;

	// Set the number of indices in the index array.
	m_indexCount = num_particles_;

	// Create the vertex array.
	vertices = new ParticlePoint[m_vertexCount];

	// Create the index array.
	indices = new unsigned long[m_indexCount];

	int i = 0;
	for (int particle = 0; particle < num_particles_; ++particle) {
		vertices[i].position = XMFLOAT3(XMVectorGetX(particles_[particle]->position_), XMVectorGetY(particles_[particle]->position_), XMVectorGetZ(particles_[particle]->position_));
		vertices[i].size = XMFLOAT2(particles_[particle]->size_, (particles_[particle]->duration_remaining_ / particles_[particle]->initial_duration_));
		indices[i] = i;
		i++;
	}

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(ParticlePoint)* m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long)* m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}

void ParticleContainer::UpdateParticle(float dt, int particle) {
	float perentage_duration = (particles_[particle]->duration_remaining_ / particles_[particle]->initial_duration_);
	// Move particle, based on velocity.
	particles_[particle]->set_position(
		particles_[particle]->position_
		+ (particles_[particle]->velocity_ * dt
			// reduce speed based on duration of particle
			* perentage_duration)
		+ XMVectorSet(settings_.gravity_x, settings_.gravity_y, settings_.gravity_z, 1.0f));

	// change size based on current size and end size
	particles_[particle]->set_size(LittleLot::Lerp(particles_[particle]->size_, particles_[particle]->end_size_, 1.0f - perentage_duration));

}



