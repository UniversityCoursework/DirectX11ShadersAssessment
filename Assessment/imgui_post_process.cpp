#include "imgui_post_process.h"

#include "imgui_utils.h"

#include "ChromaticAberationProccess.h"
#include "NoiseProcess.h"


void ExposeChromaticAberation(ChromaticProcess& chromatic_process) {

	ImGui::Text("Chroamtic Aberation Effect");
	ImGui::TextWrapped("Current values give a nice chromatic aberation effect. \n\nIncrease the power to make it more obvious, and mess around with the other settings to change the effect.");

	XMFLOAT3 displacement = chromatic_process.DisplacementScale();
	ImGui::SliderFloat3("Displacement", &displacement.x, 0.0f, 2.0f, "%.3f");
	chromatic_process.SetDisplacementScale(displacement);

	float base_radius = chromatic_process.BaseRadius();
	float falloff_radius = chromatic_process.FalloffRadius();

	ImGui::DragFloatRange2("Radius", &base_radius, &falloff_radius, 0.1f, 0.0f, 0.0f, "Base: %.2f", "Falloff: %.2f");

	chromatic_process.SetBaseRadius(base_radius);
	chromatic_process.SetFalloffRadius(falloff_radius);

	float chroma_power = chromatic_process.ChromaPower();
	ImGui::SliderFloat("Power", &chroma_power, 0.0f, 100.0f, "%.3f");
	chromatic_process.SetChromaPower(chroma_power);
}

void ExposeNoise(NoiseProcess& noise_process) {
	ImGui::Text("Noise Effects");
	ImGui::TextWrapped("Selection of 3 simple GPU driven noise effects.");

	int selected = noise_process.NoiseType();
	ImGui::Combo("##Select", &selected, "Noise 1 Effect\0Noise 2 Effect\0Noise 3 Effect\0");
	noise_process.SetNoiseType((NoiseProcessType)selected);

	float noise_level = noise_process.NoiseLevel();
	ImGui::SliderFloat("Noise Level", &noise_level, -0.06f, 0.06f, "%.3f");
	noise_process.SetNoiseLevel(noise_level);
}

void ImGui::ExposePostProcess(PostProcess & post_process) {
	switch (post_process.Type()) {

		case kNothing:

			break;
		case kChromaticAberation:
		{
			ChromaticProcess& chromatic_process = dynamic_cast<ChromaticProcess&>(post_process);
			ExposeChromaticAberation(chromatic_process);
			break;
		}
		case kNoise:
		{
			NoiseProcess& noise_process = dynamic_cast<NoiseProcess&>(post_process);
			ExposeNoise(noise_process);
			break;
		}
		break;
		default:
			break;

	}
}

void ImGui::PostProcessEditorWindow(bool * is_open, std::vector<PostProcess*> post_process_effects, PostProcessType &selected_process) {
	static bool has_generated_post_proccess_strings_ = false;
	static std::vector<std::string> post_process_string_enums_;

	if (!has_generated_post_proccess_strings_) {
		for each (PostProcess* post_process in post_process_effects) {
			post_process_string_enums_.push_back(PostProcessTypeToString(post_process->Type()));
		}
		has_generated_post_proccess_strings_ = true;
	}


	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(300, io.DisplaySize.y / 2.0f - 20);
	ImVec2 window_pos = ImVec2(io.DisplaySize.x - window_size.x, io.DisplaySize.y / 2.0f);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if (!ImGui::Begin("Post Proccess Editor", is_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}

	ImGui::Combo("Select", ((int*)&(selected_process)), post_process_string_enums_);

	ExposePostProcess(*post_process_effects[selected_process]);

	ImGui::End();

}
