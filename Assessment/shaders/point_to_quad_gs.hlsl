cbuffer MatrixBuffer : register(cb0) {
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer PointToQuadBuffer : register(cb1) {
	// 0 - gs, offset up.
	// 1 - ps, 0-texture, 1-colour.
	int4 type;

	float4 colour;
};

struct InputType {
	float4 position : POSITION;
	float2 size : TEXCOORD0;
};

struct OutputType {
	float4 position : SV_POSITION;
	float2 size : TEXCOORD0;
	float2 tex : TEXCOORD1;
};

[maxvertexcount(6)]
void main(point InputType input[1], inout TriangleStream<OutputType> triStream) {
	// Simple small little Array for layout of a quad.
	const float3 quad_layout[] = {
		{ -0.5f, 0.5f, 0.0f },
		{ -0.5f, -0.5f, 0.0f },
		{ 0.5f, -0.5f, 0.0f },
		{ -0.5f, 0.5f, 0.0f },
		{ 0.5f, -0.5f, 0.0f },
		{ 0.5f, 0.5f, 0.0f },
	};
	const float size = input[0].size.x;

	input[0].position.w = 1.0f;

	float3 right = float3(viewMatrix._11, viewMatrix._21, viewMatrix._31);

	// Remove Uninitialized warning caused by for loop later. ( silly but best to remove errors)
	float4 worldPositions[6] = {
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
	};

	float3 up = float3(0.0f, 1.0f, 0.0f);
	// if is_offset_by_up
	if (type[0]) {
		// Use the camera's up vector from the view matrix.
		up = float3(viewMatrix._12, viewMatrix._22, viewMatrix._32);
	}
	// Generate a quad lined up to the camera's right vector, and default up or camera's up.
	worldPositions[0] = input[0].position + float4(right * quad_layout[0].x * size, 0.0f) + float4(up * quad_layout[0].y * size, 0.0f);
	worldPositions[1] = input[0].position + float4(right * quad_layout[1].x * size, 0.0f) + float4(up * quad_layout[1].y * size, 0.0f);
	worldPositions[2] = input[0].position + float4(right * quad_layout[2].x * size, 0.0f) + float4(up * quad_layout[2].y * size, 0.0f);
	worldPositions[3] = input[0].position + float4(right * quad_layout[3].x * size, 0.0f) + float4(up * quad_layout[3].y * size, 0.0f);
	worldPositions[4] = input[0].position + float4(right * quad_layout[4].x * size, 0.0f) + float4(up * quad_layout[4].y * size, 0.0f);
	worldPositions[5] = input[0].position + float4(right * quad_layout[5].x * size, 0.0f) + float4(up * quad_layout[5].y * size, 0.0f);

	 // Remove Uninitialized warning caused by for loop later. ( silly but best to remove errors)
	float2 textureCoords[6] = {
		{ 0, 0 },
		{ 0, 1 },
		{ 1, 1 },
		{ 0, 0 },
		{ 1, 1 },
		{ 1, 0 },
	};

	OutputType output;
	int i = 0;
	// First Triangle
	for (i = 0; i < 3; i++) {
		// Calculate the position of the vertex against the world, view, and projection matrices.
		output.position = mul(worldPositions[i], worldMatrix);
		output.position = mul(output.position, viewMatrix);
		output.position = mul(output.position, projectionMatrix);
		output.tex = textureCoords[i];
		output.size = input[0].size;
		triStream.Append(output);
	}
	// Second Triangle
	triStream.RestartStrip();
	for (i = 3; i < 6; i++) {
		output.position = mul(worldPositions[i], worldMatrix);
		output.position = mul(output.position, viewMatrix);
		output.position = mul(output.position, projectionMatrix);
		output.tex = textureCoords[i];
		output.size = input[0].size;
		triStream.Append(output);
	}
	triStream.RestartStrip();
	
}