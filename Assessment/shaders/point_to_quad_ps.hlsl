Texture2D shaderTexture : register(t0);
SamplerState SampleType : register(s0);

cbuffer PointToQuadBuffer : register(cb0) {
    // 0 - gs, offset up.
    // 1 - ps, 0-texture, 1-colour.
    int4 type;

    float4 colour;
};

struct InputType {
    float4 position : SV_POSITION;
    float2 size : TEXCOORD0;
    float2 tex : TEXCOORD1;
};

float4 main(InputType input) : SV_TARGET {
    float4 textureColor = colour;
    // Apply the texture if it has one.
    if (type[1]) {
        textureColor = shaderTexture.Sample(SampleType, input.tex);
    }
    // Apply the calculated alpha, to allow for alpha/additive fade.
    textureColor *= input.size.y;
    return textureColor;
}