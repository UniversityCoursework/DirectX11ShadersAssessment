Texture2D shaderTexture : register(t0);
SamplerState SampleType : register(s0);

// If changed make sure is also changed in Light shader, and enough lights are set up. (Even if just turned off.)
#define MAXLIGHTS 3

cbuffer LightBuffer : register(cb0) {
	float4 ambientColor[MAXLIGHTS];
	float4 diffuseColor[MAXLIGHTS];
	float4 specularColor[MAXLIGHTS];
	// position.xyz, specularpower.w
	float4 lightPosition[MAXLIGHTS];
	// attenuation
	// constant,linear,quad,range
	float4 attenuation[MAXLIGHTS];
	float4 lightDirection[MAXLIGHTS];
	int4 type[MAXLIGHTS];
};

struct InputType {
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 viewDirection : TEXCOORD1;
	float3 position3D : TEXCOORD2;
};

float4 main(InputType input) : SV_TARGET
{
	float4 textureColor = float4(1.f, 1.f, 1.f, 1.f);
	float lightIntensity;
	float3 lightDir;
	float3 viewDir;
	float4 color = float4(0.f, 0.f, 0.f, 1.f);
	float3 reflection;
	float3 halfwayDir;
	float4 specular;
	float4 finalSpec = float4(0.f,0.f,0.f,1.f);
	float4 diffuseComp = float4(0.f, 0.f, 0.f, 1.f);

	float distance;
	float finalAttenuation;

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColor = shaderTexture.Sample(SampleType, input.tex);

	for (int i = 0; i < MAXLIGHTS; i++) {

		switch (type[i].x) {
			case 0:	// kAmbient
				{
					color += ambientColor[i];
					break;
				}
			case 1:		//kPoint
				{
					lightDir = input.position3D - lightPosition[i].xyz;
					distance = length(lightDir);
					if (distance < attenuation[i].w) {
						lightDir = normalize(lightDir);
						// Calculate the amount of light on this pixel.
						lightIntensity = dot(input.normal, -lightDir);
						if (lightIntensity > 0.0f) {
							viewDir = normalize(input.viewDirection);
							lightIntensity = saturate(lightIntensity);
							// Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
							diffuseComp = (diffuseColor[i] * lightIntensity);
							// calculate attenuation
							finalAttenuation = 1.0f / (attenuation[i].x + attenuation[i].y *distance + attenuation[i].z * pow(distance, 2));
							color += (diffuseComp * finalAttenuation);
							if (lightPosition[i].w > 0) {
								// blinn-phong			
								halfwayDir = normalize(-lightDir + viewDir);
								specular = pow(saturate(dot(halfwayDir, input.normal)), lightPosition[i].w);

								specular *= lightIntensity;
								// sum up specular light
								finalSpec += ((specularColor[i] * specular) * finalAttenuation);
							}
						}
					}
					break;
				}
			case 2:		//kDirectional
				{
					lightDir = normalize(lightDirection[i].xyz);
					// Calculate the amount of light on this pixel.
					lightIntensity = dot(input.normal, -lightDir);
					if (lightIntensity > 0.0f) {
						viewDir = normalize(input.viewDirection);
						lightIntensity = saturate(lightIntensity);
						// Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
						color += (diffuseColor[i] * lightIntensity);
						if (lightPosition[i].w > 0) {
							// blinn-phong			
							halfwayDir = normalize(-lightDir + viewDir);
							specular = pow(saturate(dot(halfwayDir, input.normal)), lightPosition[i].w);

							specular *= lightIntensity;
							// sum up specular light
							finalSpec += ((specularColor[i] * specular));
						}
					}
					break;
				}
			case 3:		//kOff
				break;
		}
	}
	// add the texture colour
	color = color * textureColor;
	// add specular
	color += finalSpec;    
	return color;
}