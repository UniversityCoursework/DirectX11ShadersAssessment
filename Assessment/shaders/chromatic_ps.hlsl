Texture2D sceneTexture : register(t0);

SamplerState Sampler0 : register(s0);

cbuffer ChromaticBuffer : register(cb0) {
    float3 displacementScale;
    float baseRadius;
    float falloffRadius;
    float chromaPower;
    float2 padding;
};


struct InputType {
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

float4 main(InputType input) : SV_TARGET {
    float4 textureColor;

    // Calculate distance from centre.
    float dist = distance(input.tex, float2(0.5, 0.5));
    // Lerp between base radius and falloff, clamping to these values.
    float f = smoothstep(baseRadius, falloffRadius, dist);
    // Calculate Chroma rgb, .
    float3 chroma = pow(abs(f + displacementScale), abs(chromaPower));
       
    // Offset texture coords by chroma, and ensure still valid UV coords. 
    // i.e. centred on the centre of the screen, but each  rgb is offset dependent on screen position.
    float2 tr = ((2.0 * input.tex - 1.0) * chroma.r) * 0.5 + 0.5;
    float2 tg = ((2.0 * input.tex - 1.0) * chroma.g) * 0.5 + 0.5;
    float2 tb = ((2.0 * input.tex - 1.0) * chroma.b) * 0.5 + 0.5;

    // Sample texture and new UV coords, for rgb.
    textureColor.r = sceneTexture.Sample(Sampler0, tr).r;
    textureColor.g = sceneTexture.Sample(Sampler0, tg).g;
    textureColor.b = sceneTexture.Sample(Sampler0, tb).b;

    // Makes sure alpha is full.
    textureColor.a = 1.0f;
    return textureColor;
}