struct InputType {
    float4 position : POSITION;
    float2 size : TEXCOORD0;
};

InputType main(InputType input) {
    // Just pass info to the geometry shader.
    return input;
}