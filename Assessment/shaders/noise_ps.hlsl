Texture2D sceneTexture : register(t0);

SamplerState Sampler0 : register(s0);

cbuffer NoiseBuffer : register(cb0) {
    // [0] - type of noise -1,2,3.
    int4 type;
    float noiseLevel;
    float3 padding;
};

struct InputType {
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

// From http://gamedev.stackexchange.com/questions/32681/random-number-hlsl
// Some Cool noise calculations with little overhead, struggling to find original source.
// Based on Canonical OpenGL Random
// Possible origins: 
// "On generating random numbers, with help of y= [(a+x)sin(bx)] mod 1", W.J.J. Rey, 22nd European Meeting of Statisticians and the 7th Vilnius Conference on Probability Theory and Mathematical Statistics, August 1998
// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.141.3911&rep=rep1&type=pdf
// https://books.google.co.uk/books?id=QTQk8tXrHKUC&pg=PA323&dq=rey+22nd+European+Meeting+of+Statisticians+and+the+7th+Vilnius+Conference+on+Probability+Theory+and+Mathematical+Statistics,+August+1998&redir_esc=y#v=onepage&q=rey%2022nd%20European%20Meeting%20of%20Statisticians%20and%20the%207th%20Vilnius%20Conference%20on%20Probability%20Theory%20and%20Mathematical%20Statistics%2C%20August%201998&f=false
float noise_1(in float2 uv) {
    float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
    return abs(noise.x + noise.y) * 0.5;
}

float2 noise_2(in float2 uv) {
    float noiseX = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
    float noiseY = sqrt(1 - noiseX * noiseX);
    return float2(noiseX, noiseY);
}

float2 noise_3(in float2 uv) {
    float noiseX = (frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453));
    float noiseY = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
    return float2(noiseX, noiseY);
}

float4 main(InputType input) : SV_TARGET {
    float4 textureColor;
    
    float2 offsets = float2(0, 0);
    if (type[0] == 0) // noise 1
    {
        float distortion = noise_1(input.tex);
        offsets = distortion * noiseLevel;
    }
    if (type[0] == 1) // noise 2
    {
        float2 distortion = noise_2(input.tex);
        offsets.x = distortion.x * noiseLevel;
        offsets.y = distortion.y * noiseLevel;
    }
    if (type[0] == 2) // noise 3
    {
        float2 distortion = noise_3(input.tex);
        offsets.x = distortion.x * noiseLevel;
        offsets.y = distortion.y * noiseLevel;
    }
    
    float4 leftColor = sceneTexture.Sample(Sampler0, input.tex + offsets);
    float4 rightColor = sceneTexture.Sample(Sampler0, input.tex - offsets);
    
    textureColor = lerp(leftColor, rightColor, 0.5f);
    return textureColor;
}