
cbuffer TessellationBuffer : register(cb0) {	
    float nearClip;
    float farClip;
    float minTess;
    float maxTess;

    float tessAmp;
    float3 padding;
}

cbuffer CameraBuffer : register(cb1) {
    float3 cameraPosition;
    float gameTime;
};

struct InputType {
    float3 position : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

struct ConstantOutputType {
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};

struct OutputType {
    float3 position : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

// https://developer.nvidia.com/content/dynamic-hardware-tessellation-basics
// Calculate the tesselation factor based on distance from camera for each quad,Enables varaible tesselation based on distance.
float CalculateTesselationAmount(float3 centrePosition) {
    float dist = distance(centrePosition, cameraPosition);

    float tessScale = 1 - (dist - nearClip) / (farClip - nearClip);
    return clamp(tessScale * tessAmp, minTess, maxTess);
}

ConstantOutputType PatchConstantFunction(InputPatch<InputType, 4> inputPatch, uint patchId : SV_PrimitiveID) {
    ConstantOutputType output;
    
    // Weird order due to z layout of points for edges.
    // 0-1
    //  /
    // 2-3
    // Set the tessellation factors for the outside edges of the quad, based on mid point of edges.
    // Make sure matching edges are tesselated equally to stop cracks forming.
    output.edges[0] = CalculateTesselationAmount((inputPatch[0].position + inputPatch[1].position) / 2);
    output.edges[1] = CalculateTesselationAmount((inputPatch[1].position + inputPatch[3].position) / 2);
    output.edges[2] = CalculateTesselationAmount((inputPatch[3].position + inputPatch[2].position) / 2);
    output.edges[3] = CalculateTesselationAmount((inputPatch[0].position + inputPatch[2].position) / 2);
    // Set the tessellation factors for the inside edges of the quad, based on centre of quad.
    output.inside[0] = CalculateTesselationAmount((inputPatch[0].position + inputPatch[1].position + inputPatch[2].position + inputPatch[3].position) / 4);
    output.inside[1] = CalculateTesselationAmount((inputPatch[0].position + inputPatch[1].position + inputPatch[2].position + inputPatch[3].position) / 4);	

    return output;
}

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("PatchConstantFunction")]
OutputType main(InputPatch<InputType, 4> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID) {
    OutputType output;

    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    // Set the input color as the output color.
    output.tex = patch[pointId].tex;
    output.normal = patch[pointId].normal;

    return output;
}