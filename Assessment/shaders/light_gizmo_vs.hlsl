// Very Basic Editor Shader.
// Used to render light positions with diffuse colour.

cbuffer MatrixBuffer : register(cb0) {
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

struct InputType {
	float4 position : POSITION;
};

struct OutputType {
	float4 position : SV_POSITION;
};

OutputType main(InputType input) {
	OutputType output;

	// Change the position vector to be 4 units for proper matrix calculations.
	input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	return output;
}