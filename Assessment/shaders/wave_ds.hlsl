Texture2D texture0 : register(t0);

static const float PI = 3.14159265f;

cbuffer MatrixBuffer : register(cb0) {
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer WaveBuffer : register(cb1) {
	float amplitude;
	float steepness;
	float waveLength;
	float2 waveDirection;
	float3 padding;
};


cbuffer CameraBuffer : register(cb2) {
	float3 cameraPosition;
	float gameTime;
};

struct ConstantOutputType {
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
};

struct InputType {
	float3 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType {
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 viewDirection : TEXCOORD1;
	float3 position3D : TEXCOORD2;
};

// Using https://developer.nvidia.com/gpugems/GPUGems/gpugems_ch01.html
// Gerstener wave equation 9.
// q = steepness
// a = amplitude
//
// cos section = waveCos
// sin section = waveSin
// w = wavePeroid
// &t = gameTime
// D.x = can just use direction of wave as directional
float3 CalculateWavePosition(float3 vertexPosition) {

	float2 directionOfWave = normalize(waveDirection);
	float wavePeriod = 2 * PI / waveLength;
	
	// shared varaibles
	float3 resultPosition = float3(0, 0, 0);
	float dotVertexDirection;
	float waveCos;
	float waveSin;

	///* Wave on x-z plane.	
	dotVertexDirection = dot(float2(vertexPosition.x, vertexPosition.z), directionOfWave);
	waveCos = cos(wavePeriod * dotVertexDirection + gameTime);
	waveSin = sin(wavePeriod * dotVertexDirection + gameTime);

	resultPosition += float3(steepness * amplitude * waveCos * directionOfWave.x,
							amplitude * waveSin,
							steepness * amplitude * waveCos * directionOfWave.y);
	//*/

	///* Wave on x-y plane.
	dotVertexDirection = dot(float2(vertexPosition.x, vertexPosition.y), directionOfWave);
	waveCos = cos(wavePeriod * dotVertexDirection + gameTime);
	waveSin = sin(wavePeriod * dotVertexDirection + gameTime);

	resultPosition += float3(steepness * amplitude * waveCos * directionOfWave.x,
							steepness * amplitude * waveCos * directionOfWave.y,
							amplitude * waveSin);
	//*/

	///* Wave on y-z plane.
	dotVertexDirection = dot(float2(vertexPosition.y, vertexPosition.z), directionOfWave);
	waveCos = cos(wavePeriod * dotVertexDirection + gameTime);
	waveSin = sin(wavePeriod * dotVertexDirection + gameTime);

	resultPosition += float3(amplitude * waveSin,
							steepness * amplitude * waveCos * directionOfWave.x,
							steepness * amplitude * waveCos * directionOfWave.y);	
	//*/

	resultPosition = resultPosition / 3;
	return resultPosition;
}

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch) {
	float4 worldPosition;
	float3 vertexPosition;
	float3 normalDirection;
	OutputType output;

	// Position.
	// Recalculate the position based on interpolation between patchs.
	float3 v1 = lerp(patch[0].position, patch[1].position, 1 - uvwCoord.y);
	float3 v2 = lerp(patch[2].position, patch[3].position, 1 - uvwCoord.y);
	vertexPosition = lerp(v1, v2, uvwCoord.x);

	vertexPosition += CalculateWavePosition(vertexPosition);

	// Calculate the position of the new vertex against the world, view, and projection matrices.
	worldPosition = mul(float4(vertexPosition, 1.0f), worldMatrix);
	output.position = mul(worldPosition, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	// Tex Coords.
	// Recalculate the UVcoord based on interpolation between patchs.
	float2 t1 = lerp(patch[0].tex, patch[1].tex, 1 - uvwCoord.y);
	float2 t2 = lerp(patch[2].tex, patch[3].tex, 1 - uvwCoord.y);
	output.tex = lerp(t1, t2, uvwCoord.x);

	
	// Sorta Cheating with normals, was doing with gernster method, but seemed most the time there was not a noticable difference...
	// or it was horribly wrong :/
	// (directionOfWave.x*wavePeriod*amplitude*waveCos,
	// directionOfWave.x*wavePeriod*amplitude*waveCos,
	// 1 - (steepness*wavePeriod*amplitude*waveSin))
	// Normals.
	// Recalculate the Normals based on interpolation between patchs.
	float3 n1 = lerp(patch[0].normal, patch[1].normal, 1 - uvwCoord.y);
	float3 n2 = lerp(patch[2].normal, patch[3].normal, 1 - uvwCoord.y);
		
	normalDirection = lerp(n1, n2, uvwCoord.x);

	// Calculate the normal vector against the world matrix only.
	output.normal = mul(normalDirection, (float3x3) worldMatrix);
	// Normalize the normal vector.
	output.normal = normalize(output.normal);

	// World position of the vertex, Used in lighting calculations.
	output.position3D = worldPosition.xyz;
	// Determine the viewing direction based on the position of the camera. Used in lighting calculations.
	output.viewDirection = normalize(cameraPosition.xyz - worldPosition.xyz);

	return output;
}

