#ifndef _WAVE_GAMEOBJECT_H
#define _WAVE_GAMEOBJECT_H

#include "GameObject.h"
#include "WaveShader.h"

/// <summary>
/// Wave object for modifing the tesseleted objects with waves passed through them.
/// </summary>
class WaveObject : public GameObject {
public:
	WaveObject();
	WaveObject(std::string name);
	~WaveObject();

	/// <summary>
	/// The distance at which tesselation will be at its highest.
	/// </summary>	
	void SetNearClip(float near_clip);

	/// <summary>
	/// The distance at which tesselation will be at its lowest.
	/// </summary>	
	void SetFarClip(float far_clip);

	/// <summary>
	/// The lowest amount of possible tesselation.
	/// </summary>	
	void SetMinTess(float min_tess);

	/// <summary>
	/// THe largest amount of possible tesselation.
	/// </summary>	
	void SetMaxTess(float max_tess);

	/// <summary>
	/// The amplifying amount, to increase the calculated tesselation amount by.
	/// </summary>	
	void SetTessAmp(float tess_amp);

	/// <summary>
	/// Sets the overall height of the waves to use in the wave function of the wave params.
	/// </summary>	
	void SetAmplitude(float amplitude);

	/// <summary>
	/// Controls how steep; how much it pinches at the top, in the wave function of the wave params.
	/// </summary>	
	void SetSteepness(float steepness);
	/// <summary>
	/// Set the distance between waves, in the wave function of the wave params.
	/// </summary>	
	void SetWaveLength(float wave_length);

	/// <summary>
	/// The struct of Wave parameters from this object, for use with the wave shader.
	/// </summary>	
	WaveShader::WaveParamaters* WaveParams();

	float NearClip();
	float FarClip();
	float MinTess();
	float MaxTess();
	float TessAmp();
	float Amplitude();
	float Steepness();
	float WaveLength();

private:
	WaveShader::WaveParamaters params_;
};

#endif
