#include "imgui_d3d.h"
// Extra Librarys needed for the DirectX resolution detection.
#include <sstream>
#include <vector>
#include <utility>
#include <string>
#include "imgui_utils.h"

void ImGui::ExposeWireFrameMenuItem(D3D * d3d) {
	bool wireframeMode = d3d->IsWireFrameModeOn();
	ImGui::MenuItem("Wireframe Mode", NULL, &wireframeMode);
	if (wireframeMode) {
		d3d->TurnOnWireframe();
	} else {
		d3d->TurnOffWireframe();
	}
}

void ImGui::ExposeWireFrame(D3D * d3d) {
	bool wireframeMode = d3d->IsWireFrameModeOn();
	ImGui::Checkbox("WireFrameMode", &wireframeMode);
	if (wireframeMode) {
		d3d->TurnOnWireframe();
	} else {
		d3d->TurnOffWireframe();
	}
}

void ImGui::DisplayFPS() {

	ImGui::SameLine(0);
	ImGui::PushItemWidth(160);
	ImGui::Text("FPS: %.1f, %.3f ms", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
}



bool ImGui::ExposeResolutionOptions(D3D * d3d, HWND hwnd) {
	// Ensures it only does the expensive resolution fetching once.
	static bool has_possible_resolutions_ = false;

	// Used to store all the resolutions available on this device.
	static std::vector<std::pair<int, int>> screen_resolutions_;
	// Used to store all the resolutions in Readable format for ImGui drop down list.
	static std::vector<std::string> screen_string_enums_;

	static int selected_resolution = 1;

	// Generate Resolution list.
	if (!has_possible_resolutions_) {

		IDXGIFactory* factory;
		IDXGIAdapter* adapter;
		IDXGIOutput* adapterOutput;
		DXGI_MODE_DESC* displayModeList;
		unsigned int numModes;

		// Create a DirectX graphics interface factory.
		CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

		// Use the factory to create an adapter for the primary graphics interface (video card).
		factory->EnumAdapters(0, &adapter);

		// Enumerate the primary adapter output (monitor).
		adapter->EnumOutputs(0, &adapterOutput);

		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		displayModeList = new DXGI_MODE_DESC[numModes];

		// Now fill the display mode list structures.
		adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);

		// Now go through all the display modes and find all the sposible screen widths/heights.
		for (unsigned int i = 0; i < numModes; i++) {
			// Always add the first one.
			if (screen_resolutions_.empty()) {
				screen_resolutions_.push_back(std::make_pair(displayModeList[i].Width, displayModeList[i].Height));
			} else {
				// Check the rest to see if they are unique and not already in the vector.
				// Luckily displaymodelist stores then numerically so width/height will match previous if not unique.
				if ((displayModeList[i - 1].Width != displayModeList[i].Width) &&
					(displayModeList[i - 1].Height != displayModeList[i].Height)) {
					screen_resolutions_.push_back(std::make_pair(displayModeList[i].Width, displayModeList[i].Height));
				}
			}
			// set it up to run at 800/600 first, otherwise will run at whatever the first supported resolution is.
			if (displayModeList[i].Width == 800 && displayModeList[i].Height == 600) {
				selected_resolution = screen_resolutions_.size() - 1;
			}
		}

		// Store vector full of enum names for use by ImGui.
		for each (std::pair<int, int> resolution in screen_resolutions_) {
			screen_string_enums_.push_back(std::to_string(resolution.first) + "x" + std::to_string(resolution.second));
		}

		// clean up everything.

		// Release the display mode list.
		delete[] displayModeList;
		displayModeList = 0;

		// Release the adapter output.
		adapterOutput->Release();
		adapterOutput = 0;

		// Release the adapter.
		adapter->Release();
		adapter = 0;

		// Release the factory.
		factory->Release();
		factory = 0;
		has_possible_resolutions_ = true;
	}

	bool has_resolution_change = false;
	bool fake_fullscreen = d3d->IsFakeFullScreen();
	bool fullscreen = d3d->IsFullscreen();


	// Visuals

	if (fullscreen) {
		ImGui::Checkbox("Exclusive Fullscreen", &fullscreen);
	} else {
		ImGui::Checkbox("Fullscreen Windowed", &fake_fullscreen);
	}

	if (fake_fullscreen && !fullscreen) {
		int notAvailable = 0;
		ImGui::SameLine(0);
		ImGui::PushItemWidth(160);
		ImGui::Combo("Resolution", &notAvailable, "Not Available");
	} else {
		ImGui::SameLine(0);
		ImGui::PushItemWidth(160);
		ImGui::Combo("Resolution", &selected_resolution, screen_string_enums_);
	}

	//Logic	

	if (fake_fullscreen) {
		if (d3d->TurnFullScreenWindowedOn(hwnd)) {
			has_resolution_change = true;
		}
	} else {
		if (d3d->TurnFullScreenWindowedOff(hwnd)) {
			has_resolution_change = true;
		}
		if (d3d->ChangeResolution(screen_resolutions_[selected_resolution].first, screen_resolutions_[selected_resolution].second, hwnd)) {
			has_resolution_change = true;
		}

	}
	if (!fullscreen) {
		d3d->TurnFullscreenOff();
	}

	return has_resolution_change;
}

