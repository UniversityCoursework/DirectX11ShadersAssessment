#ifndef _VAPOUR_TRAIL_SETTINGS_H
#define _VAPOUR_TRAIL_SETTINGS_H

#include "ParticleSettings.h"

/// <summary>
/// Produces a narrow vapor trail of small particles increase into puffs of smoke.
/// </summary>
class VapourTrailSettings : public ParticleSettings {
public:
	///<summary> Initializes with default settings for a particle emiter. </summary>
	VapourTrailSettings() {
		max_duration = 4.f;
		min_duration = 1.0f;

		min_x_velocity = 2.4f;
		max_x_velocity = 4.4f;
		min_y_velocity = -0.4f;
		max_y_velocity = 0.4f;
		min_z_velocity = -0.4f;
		max_z_velocity = 0.4f;

		min_start_size = 0.1f;
		max_start_size = 0.4f;
		min_end_size = 0.7f;
		max_end_size = 1.0f;

		effect_particles = 2;
	};

};
#endif

