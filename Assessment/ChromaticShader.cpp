#include "ChromaticShader.h"

ChromaticShader::ChromaticShader(ID3D11Device * device, HWND hwnd)
	: BaseShader(device, hwnd) {
	InitShader(L"shaders/chromatic_vs.hlsl", L"shaders/chromatic_ps.hlsl");
}

ChromaticShader::~ChromaticShader() {
	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	// Release the light constant buffer.
	if (chromatic_buffer_) {
		chromatic_buffer_->Release();
		chromatic_buffer_ = 0;
	}
	//Release base shader components
	BaseShader::~BaseShader();
}

void ChromaticShader::InitShader(WCHAR * vsFilename, WCHAR * psFilename) {

	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;
	D3D11_BUFFER_DESC chromaticBufferDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &matrix_buffer_);

	// Setup chromatic aberation buffer.
	// Pixel Shader.
	chromaticBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	chromaticBufferDesc.ByteWidth = sizeof(ChromaticAberationBufferType);
	chromaticBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	chromaticBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	chromaticBufferDesc.MiscFlags = 0;
	chromaticBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&chromaticBufferDesc, NULL, &chromatic_buffer_);

	// Setup Sampler State.
	// Pixel Shader.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &sample_state_);

}


void ChromaticShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
	ID3D11ShaderResourceView* scene, XMFLOAT3 displacement_scale, float base_radius, float falloff_radius, float chroma_power) {
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;

	MatrixBufferType* dataPtr;
	ChromaticAberationBufferType* chromaticPtr;

	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;

	// Setup the Matrix Buffer.
	// Domain Shader.
	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	dataPtr->world = tworld;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Set the constant buffer in the Domain Shader.
	device_context->VSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);

	// Setup chromatic aberation buffer.
	// Pixel Shader.
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(chromatic_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	chromaticPtr = (ChromaticAberationBufferType*)mappedResource.pData;
	// Set Values
	chromaticPtr->displacementScale = displacement_scale;
	chromaticPtr->baseRadius = base_radius;
	chromaticPtr->falloffRadius = falloff_radius;
	chromaticPtr->chromaPower = chroma_power;
	chromaticPtr->padding = XMFLOAT2(0, 0);

	// Unlock the constant buffer.
	device_context->Unmap(chromatic_buffer_, 0);
	bufferNumber = 0;
	// Set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(bufferNumber, 1, &chromatic_buffer_);

	// Set shader texture resource in the pixel shader.
	device_context->PSSetShaderResources(0, 1, &scene);
}

void ChromaticShader::Render(ID3D11DeviceContext * device_context, int index_count) {
	// Set the sampler state in the pixel shader.
	device_context->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(device_context, index_count);
}


