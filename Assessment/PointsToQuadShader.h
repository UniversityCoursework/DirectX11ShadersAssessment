#ifndef _POINTS_TO_QUAD_SHADER_H
#define _POINTS_TO_QUAD_SHADER_H

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Converts a point mesh into a selection of quads at teh given points.
/// Either facing the camera, or set to default up vector. They can be Colored or textured quads.
/// They will not be lit.
/// </summary>
class PointsToQuadShader : public BaseShader {
private:
	struct PointToQuadType {
		// 0 - gs, is_offset_by_up.
		// 1 - ps, 0-texture, 1-colour.
		XMINT4 type;
		XMFLOAT4 colour;
	};
public:
	PointsToQuadShader(ID3D11Device* device, HWND hwnd);
	~PointsToQuadShader();

	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* texture, bool is_offset_by_up = true);

	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		XMFLOAT4 colour, bool is_offset_by_up = true);

	void Render(ID3D11DeviceContext* device_context, int index_count);
private:
	void InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11Buffer* point_to_quad_buffer_;

	ID3D11SamplerState* sample_state_;
};

#endif
