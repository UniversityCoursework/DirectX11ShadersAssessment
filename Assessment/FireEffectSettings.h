#ifndef _FIRE_EFFECT_SETTINGS_H
#define _FIRE_EFFECT_SETTINGS_H

#include "ParticleSettings.h"

class FireEffectSettings : public ParticleSettings {
public:
	///<summary> Initializes with default settings for a particle emiter. </summary>
	FireEffectSettings() {
		max_duration = 6.f;
		min_duration = 4.0f;

		min_x_velocity = -0.6f;
		max_x_velocity = 0.6f;
		min_y_velocity = 2.6f;
		max_y_velocity = 4.f;
		min_z_velocity = -0.6f;
		max_z_velocity = 0.6f;

		gravity_x = 0.0f;
		gravity_y = -0.02f;
		gravity_z = 0.0f;

		min_start_size = 1.0f;
		max_start_size = 1.1f;
		min_end_size = 0.0f;
		max_end_size = 0.0f;

		effect_particles = 1;
	};

};
#endif

