#ifndef _PARTICLE_H
#define _PARTICLE_H

#include <d3d11.h>
#include <directxmath.h>

using namespace DirectX;

///<summary> Used only by particle containers. </summary>
class Particle {
public:
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p) {
		_mm_free(p);
	}
	~Particle() {};
	///<summary> Sets initial durations for use by container update. </summary>
	void set_duration(const float duration);
	///<summary> Sets initial size for use by container update. Makes it a square.</summary>
	void set_size(const float size);
	///<summary> Sets end size for use by container update. Size it will end as.</summary>
	void set_end_size(const float end_size);

	/// <summary>
	/// Set initial velocity of the Particle.
	/// </summary>
	void set_velocity(float x, float y, float z);
	
	/// <summary>
	/// Set the initial position of the particle.
	/// </summary>	
	void set_position(XMVECTOR position);

	float duration_remaining();
	float initial_duration();
	float size();
	float end_size();

	XMVECTOR Position();
	XMVECTOR Velocity();

	///<summary> Overides the copy operator, to allow proper copies. </summary>
	Particle& operator=(const Particle& rhs);
protected:
	// Gives the particle container class access for creating these particles, and to its protected variables.
	friend class ParticleContainer;
	Particle() {};
	float duration_remaining_;
	float initial_duration_;
	float size_;
	float end_size_;

	XMVECTOR position_;
	XMVECTOR velocity_;
};
#endif