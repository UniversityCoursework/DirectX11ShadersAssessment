#include "WaveShader.h"

WaveShader::WaveShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"shaders/wave_vs.hlsl", L"shaders/wave_hs.hlsl", L"shaders/wave_ds.hlsl", L"shaders/multi_light_ps.hlsl");
}


WaveShader::~WaveShader() {
	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	// Release the tessellation constant buffer.
	if (tessellation_buffer_) {
		tessellation_buffer_->Release();
		tessellation_buffer_ = 0;
	}

	// Release the tessellation constant buffer.
	if (wave_buffer_) {
		wave_buffer_->Release();
		wave_buffer_ = 0;
	}

	// Release the light constant buffer.
	if (light_buffer_) {
		light_buffer_->Release();
		light_buffer_ = 0;
	}

	// Release the camera constant buffer.
	if (camera_buffer_) {
		camera_buffer_->Release();
		camera_buffer_ = 0;
	}
	//Release base shader components
	BaseShader::~BaseShader();
}

void WaveShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename) {

	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC tessellationBufferDesc;
	D3D11_BUFFER_DESC waveBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_BUFFER_DESC cameraBufferDesc;

	D3D11_SAMPLER_DESC samplerDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);


	// Setup tessellation buffer.
	// Hull Shader.
	tessellationBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	tessellationBufferDesc.ByteWidth = sizeof(TessellationBufferType);
	tessellationBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	tessellationBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	tessellationBufferDesc.MiscFlags = 0;
	tessellationBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&tessellationBufferDesc, NULL, &tessellation_buffer_);


	// Setup the Matrix Buffer.
	// Domain Shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &matrix_buffer_);


	// Setup Wave buffer.
	// Domain Shader.
	waveBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	waveBufferDesc.ByteWidth = sizeof(WaveBufferType);
	waveBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	waveBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	waveBufferDesc.MiscFlags = 0;
	waveBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&waveBufferDesc, NULL, &wave_buffer_);


	// Setup the camera buffer	.
	// Domain Shader.
	cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cameraBufferDesc.ByteWidth = sizeof(CameraBufferType);
	cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cameraBufferDesc.MiscFlags = 0;
	cameraBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&cameraBufferDesc, NULL, &camera_buffer_);


	// Setup light buffer.
	// Pixel Shader.
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&lightBufferDesc, NULL, &light_buffer_);


	// Setup Sampler State.
	// Pixel Shader.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &sample_state_);

}

void WaveShader::InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename) {
	// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
	InitShader(vsFilename, psFilename);

	// Load other required shaders.
	loadHullShader(hsFilename);
	loadDomainShader(dsFilename);
}


void WaveShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, ID3D11ShaderResourceView* texture,
	WaveParamaters* wave_params, Light* light[MAXLIGHTS], XMFLOAT3 camera_position, float game_time) {
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;

	TessellationBufferType* tessPtr;
	WaveBufferType* wavePtr;
	MatrixBufferType* dataPtr;
	CameraBufferType* cameraPtr;

	LightBufferType* lightPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;


	// Setup tessellation buffer.
	// Hull Shader.	
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(tessellation_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	tessPtr = (TessellationBufferType*)mappedResource.pData;
	tessPtr->nearClip = wave_params->nearClip;
	tessPtr->farClip = wave_params->farClip;
	tessPtr->minTess = wave_params->minTess;
	tessPtr->maxTess = wave_params->maxTess;
	tessPtr->tessAmp = wave_params->tessAmp;
	tessPtr->padding = XMFLOAT3();

	// Unlock the constant buffer.
	device_context->Unmap(tessellation_buffer_, 0);
	bufferNumber = 0;
	// Set the constant buffer in the Hull Shader.
	device_context->HSSetConstantBuffers(bufferNumber, 1, &tessellation_buffer_);


	// Setup the Matrix Buffer.
	// Domain Shader.
	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	dataPtr->world = tworld;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Set the constant buffer in the Domain Shader.
	device_context->DSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);

	// Setup the wave buffer	.
	// Domain Shader.
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(wave_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	wavePtr = (WaveBufferType*)mappedResource.pData;
	// Set Values	
	wavePtr->amplitude = wave_params->amplitude;
	wavePtr->steepness = wave_params->steepness;
	wavePtr->waveLength = wave_params->waveLength;
	wavePtr->waveDirection = wave_params->waveDirection;
	wavePtr->padding = XMFLOAT3();


	// Unlock the constant buffer.
	device_context->Unmap(wave_buffer_, 0);

	bufferNumber = 1;
	// Set the constant buffer in the Domain shader
	device_context->DSSetConstantBuffers(bufferNumber, 1, &wave_buffer_);


	// Setup the camera buffer.
	// Domain Shader.. HS
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(camera_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	cameraPtr = (CameraBufferType*)mappedResource.pData;
	// Set Values
	cameraPtr->cameraPosition = camera_position;
	cameraPtr->gameTime = game_time;

	// Unlock the constant buffer.
	device_context->Unmap(camera_buffer_, 0);

	bufferNumber = 2;
	// Set the constant buffer in the Domain shader
	device_context->DSSetConstantBuffers(bufferNumber, 1, &camera_buffer_);

	bufferNumber = 1;
	// Set the constant buffer in the Hull shader
	device_context->HSSetConstantBuffers(bufferNumber, 1, &camera_buffer_);

	// Setup light buffer.
	// Pixel Shader.
	// Lock the light to the constant buffer so it can be written to.
	device_context->Map(light_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// get a pointer to the data in the constant buffer
	lightPtr = (LightBufferType*)mappedResource.pData;
	// Set Values
	for (int i = 0; i < MAXLIGHTS; i++) {
		// Copy light data into the buffer		
		lightPtr->ambient[i] = light[i]->GetAmbientColour();
		lightPtr->diffuse[i] = light[i]->GetDiffuseColour();
		lightPtr->specular[i] = light[i]->GetSpecularColour();
		lightPtr->position[i] = XMFLOAT4(light[i]->GetPosition().x, light[i]->GetPosition().y, light[i]->GetPosition().z, light[i]->GetSpecularPower());
		lightPtr->attenuation[i] = XMFLOAT4(light[i]->GetConstantFactor(), light[i]->GetLinearFactor(), light[i]->GetQuadraticFactor(), light[i]->GetRange());
		lightPtr->direction[i] = XMFLOAT4(light[i]->GetDirection().x, light[i]->GetDirection().y, light[i]->GetDirection().z, 0);
		lightPtr->type[i] = XMINT4((int)light[i]->GetType(), 0, 0, 0);
	}

	// unlock the buffer
	device_context->Unmap(light_buffer_, 0);
	bufferNumber = 0;
	// Set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(bufferNumber, 1, &light_buffer_);

	// Set shader texture resource in the pixel shader.
	device_context->PSSetShaderResources(0, 1, &texture);
	device_context->DSSetShaderResources(0, 1, &texture);
}

void WaveShader::Render(ID3D11DeviceContext* device_context, int index_count) {
	// Set the sampler state in the pixel shader.	
	device_context->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(device_context, index_count);
}
