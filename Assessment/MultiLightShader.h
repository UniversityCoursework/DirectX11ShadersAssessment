#ifndef _MULTILIGHT_SHADER_H
#define _MULTILIGHT_SHADER_H

#include "../DXFramework/BaseShader.h"
#include "../DXFramework/Light.h"

using namespace std;
using namespace DirectX;

#define MAXLIGHTS 3

/// <summary>
/// Multiple Light Shader.
/// Using light type, to calculate the correct type of lighting for each light.
/// </summary>
class MultiLightShader : public BaseShader {
private:
	struct LightBufferType {
		XMFLOAT4 ambient[MAXLIGHTS];
		XMFLOAT4 diffuse[MAXLIGHTS];
		XMFLOAT4 specular[MAXLIGHTS];
		XMFLOAT4 position[MAXLIGHTS];
		XMFLOAT4 attenuation[MAXLIGHTS];
		XMFLOAT4 direction[MAXLIGHTS];
		XMINT4 type[MAXLIGHTS];
	};

	struct CameraBufferType {
		XMFLOAT3 cameraPosition;
		float padding;
	};

public:

	MultiLightShader(ID3D11Device* device, HWND hwnd);
	~MultiLightShader();

	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, 
		ID3D11ShaderResourceView* texture, Light* light[MAXLIGHTS], XMFLOAT3 camera_position);
	void Render(ID3D11DeviceContext* device_context, int index_count);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11SamplerState* sample_state_;
	ID3D11Buffer* light_buffer_;
	ID3D11Buffer* camera_buffer_;

};

#endif