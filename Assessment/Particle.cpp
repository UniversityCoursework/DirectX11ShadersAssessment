#include "Particle.h"

void Particle::set_duration(const float duration) {
	duration_remaining_ = duration;
	initial_duration_ = duration;
}

void Particle::set_size(const float size) {
	size_ = size;
}

void Particle::set_end_size(const float end_size) {
	end_size_ = end_size;
}

void Particle::set_velocity(float x, float y, float z) {
	velocity_ = XMVectorSet(x, y, z, 0.0f);
}

void Particle::set_position(XMVECTOR position) {
	position_ = position;
}

float Particle::duration_remaining() {
	return duration_remaining_;
}

float Particle::initial_duration() {
	return initial_duration_;
}

float Particle::size() {
	return size_;
}

float Particle::end_size() {
	return end_size_;
}

XMVECTOR Particle::Position() {
	return position_;
}

XMVECTOR Particle::Velocity() {
	return velocity_;
}

Particle & Particle::operator=(const Particle & rhs) {
	position_ = rhs.position_;
	velocity_ = rhs.velocity_;
	duration_remaining_ = rhs.duration_remaining_;
	initial_duration_ = rhs.initial_duration_;
	size_ = rhs.size_;
	end_size_ = rhs.end_size_;
	return *this;
}
