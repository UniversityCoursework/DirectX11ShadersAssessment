#ifndef _WAVE_SHADER_H
#define _WAVE_SHADER_H

#include "../DXFramework/BaseShader.h"
#include "../DXFramework/Light.h"
#include "MultiLightShader.h"	// For max Lights :/

using namespace std;
using namespace DirectX;


/// <summary>
/// Tesselates object based on quads. 
/// Applies 3 Plane Wave function through all vertices.
/// Applies Multi lighting effect to the object.
/// </summary>
class WaveShader : public BaseShader {
private:
	struct TessellationBufferType {
		float nearClip;
		float farClip;
		float minTess;
		float maxTess;

		float tessAmp;
		XMFLOAT3 padding;
	};

	struct WaveBufferType {
		float amplitude;
		float steepness;
		float waveLength;
		XMFLOAT2 waveDirection;
		XMFLOAT3 padding;
	};

	struct LightBufferType {
		XMFLOAT4 ambient[MAXLIGHTS];
		XMFLOAT4 diffuse[MAXLIGHTS];
		XMFLOAT4 specular[MAXLIGHTS];
		XMFLOAT4 position[MAXLIGHTS];
		XMFLOAT4 attenuation[MAXLIGHTS];
		XMFLOAT4 direction[MAXLIGHTS];
		XMINT4 type[MAXLIGHTS];
	};

	struct CameraBufferType {
		XMFLOAT3 cameraPosition;
		float gameTime;
	};
public:
	// Contains all the settings for wave generation.
	// Has some basic default values.
	struct WaveParamaters {
		float nearClip = 2.0f;
		float farClip = 100.0f;
		// 1-64.0f
		float minTess = 1.0f;
		// 1-64.0f
		float maxTess = 64.0f;
		// 1-64.0f
		float tessAmp = 64.0f;
		// 0 - 1 sensible values.
		float amplitude = 0.3f;
		// 0 - 1 sensible values.
		float steepness = 0.1f;
		// 0 - 1 sensible values.
		float waveLength = 0.45f;
		// Is normalized in shader. (-1) -> 1 on each axis.
		XMFLOAT2 waveDirection = XMFLOAT2(0, 1);
	};

	WaveShader(ID3D11Device* device, HWND hwnd);
	~WaveShader();

	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, ID3D11ShaderResourceView* texture,
		WaveParamaters* wave_params, Light* light[MAXLIGHTS], XMFLOAT3 camera_position, float game_time);
	void Render(ID3D11DeviceContext* device_context, int index_count);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
	void InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);
private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11Buffer* tessellation_buffer_;
	ID3D11Buffer* wave_buffer_;
	ID3D11Buffer* light_buffer_;
	ID3D11Buffer* camera_buffer_;

	ID3D11SamplerState* sample_state_;
};

#endif
