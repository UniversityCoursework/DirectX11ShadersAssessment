#include "EmitterObject.h"

EmitterObject::EmitterObject()
	: GameObject() {
	type_ = kEmitter;
	is_offset_by_up_ = true;
	particle_emitter_ = nullptr;
}

EmitterObject::EmitterObject(std::string name)
	: EmitterObject() {
	name_ = name;
}

EmitterObject::~EmitterObject() {
	mesh_ = nullptr;
	particle_emitter_ = nullptr;
}

void EmitterObject::SetOffsetByUp(bool offset) {
	is_offset_by_up_ = offset;
}

void EmitterObject::SetEmitter(ParticleEmitter * particle_emitter) {
	particle_emitter_ = particle_emitter;
	mesh_ = particle_emitter;
}

void EmitterObject::SetEmissionRate(float rate) {
	particle_emitter_->SetEmissionRate(rate);
}

void EmitterObject::SetPosition(float x, float y, float z) {
	particle_emitter_->SetPosition(x, y, z);
}

bool EmitterObject::IsOffsetByUp() {
	return is_offset_by_up_;
}

void EmitterObject::TurnOn() {
	particle_emitter_->TurnOn();
}

void EmitterObject::TurnOff() {
	particle_emitter_->TurnOff();
}

bool EmitterObject::IsOn() {
	return particle_emitter_->IsOn();
}

float EmitterObject::EmissionRate() {
	return particle_emitter_->EmissionRate();
}

XMFLOAT3 EmitterObject::Position() {
	return particle_emitter_->Position();
}

int EmitterObject::NumOfParticles() {
	return particle_emitter_->NumOfParticles();
}

int EmitterObject::MaxNumOfParticles() {
	return particle_emitter_->MaxNumOfParticles();
}

ParticleSettings * EmitterObject::EmitterParticleSettings() {
	return particle_emitter_->ContainerParticleSettings();
}
