#include "MultiLightShader.h"

MultiLightShader::MultiLightShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"shaders/multi_light_vs.hlsl", L"shaders/multi_light_ps.hlsl");
}

MultiLightShader::~MultiLightShader() {
	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	// Release the light constant buffer.
	if (light_buffer_) {
		light_buffer_->Release();
		light_buffer_ = 0;
	}

	// Release the camera constant buffer.
	if (camera_buffer_) {
		camera_buffer_->Release();
		camera_buffer_ = 0;		
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void MultiLightShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename) {
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_BUFFER_DESC cameraBufferDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &matrix_buffer_);

	// Create a texture sampler state description.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &sample_state_);

	// Setup light buffer
	// Setup the description of the light dynamic constant buffer that is in the pixel shader.
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&lightBufferDesc, NULL, &light_buffer_);

	// Setup the camera buffer	
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cameraBufferDesc.ByteWidth = sizeof(CameraBufferType);
	cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cameraBufferDesc.MiscFlags = 0;
	cameraBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&cameraBufferDesc, NULL, &camera_buffer_);

}

void MultiLightShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, 
	ID3D11ShaderResourceView* texture, Light *light[MAXLIGHTS], XMFLOAT3 camera_position) {

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	LightBufferType* lightPtr;
	CameraBufferType* cameraPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;


	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;

	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);

	// Additional
	// Send camera data to vertex shader
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(camera_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	cameraPtr = (CameraBufferType*)mappedResource.pData;
	cameraPtr->cameraPosition = camera_position;
	cameraPtr->padding = 0.0f;

	// Unlock the constant buffer.
	device_context->Unmap(camera_buffer_, 0);

	bufferNumber = 1;
	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(bufferNumber, 1, &camera_buffer_);


	//Additional
	// Send light data to pixel shader

	// Lock the light to the constant buffer so it can be written to.
	device_context->Map(light_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// get a pointer to the data in the constant buffer
	lightPtr = (LightBufferType*)mappedResource.pData;

	for (int i = 0; i < MAXLIGHTS; i++) {
		// Copy light data into the buffer		
		lightPtr->ambient[i] = light[i]->GetAmbientColour();
		lightPtr->diffuse[i] = light[i]->GetDiffuseColour();
		lightPtr->specular[i] = light[i]->GetSpecularColour();
		lightPtr->position[i] = XMFLOAT4(light[i]->GetPosition().x, light[i]->GetPosition().y, light[i]->GetPosition().z, light[i]->GetSpecularPower());
		lightPtr->attenuation[i] = XMFLOAT4(light[i]->GetConstantFactor(), light[i]->GetLinearFactor(), light[i]->GetQuadraticFactor(), light[i]->GetRange());
		lightPtr->direction[i] = XMFLOAT4(light[i]->GetDirection().x, light[i]->GetDirection().y, light[i]->GetDirection().z, 0);
		lightPtr->type[i] = XMINT4((int)light[i]->GetType(), 0, 0, 0);
	}

	// unlock the buffer
	device_context->Unmap(light_buffer_, 0);
	bufferNumber = 0;
	// set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(bufferNumber, 1, &light_buffer_);

	// Set shader texture resource in the pixel shader.
	device_context->PSSetShaderResources(0, 1, &texture);
}

void MultiLightShader::Render(ID3D11DeviceContext* device_context, int index_count) {
	// Set the sampler state in the pixel shader.
	device_context->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(device_context, index_count);
}



