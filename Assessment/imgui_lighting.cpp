#include "imgui_lighting.h"
#include <string>
void ExposePosition(Light* light) {
	XMFLOAT3 position = light->GetPosition();
	ImGui::DragFloat3("Position", &position.x, 0.1f);
	light->SetPosition(position.x, position.y, position.z);
}

void ExposeDirection(Light* light) {
	XMFLOAT3 direction = light->GetDirection();
	ImGui::SliderFloat3("Direction", &direction.x, -1.0f, 1.0f);
	light->SetDirection(direction.x, direction.y, direction.z);
}

void ExposeAttenuation(Light* light) {
	ImGui::Text("Attenuation");
	float AccessValue = light->GetConstantFactor();
	ImGui::SliderFloat("Constant", &AccessValue, 0.0f, 10.f, "Constant : %.3f");
	light->SetConstantFactor(AccessValue);

	AccessValue = light->GetLinearFactor();
	ImGui::SliderFloat("Linear", &AccessValue, 0.0f, 10.f, "Linear : %.3f");
	light->SetLinearFactor(AccessValue);

	AccessValue = light->GetQuadraticFactor();
	ImGui::SliderFloat("Quadratic", &AccessValue, 0.0f, 1.f, "Quadratic : %.3f");
	light->SetQuadraticFactor(AccessValue);

	AccessValue = light->GetRange();
	ImGui::SliderFloat("Range", &AccessValue, 0.0f, 100.f, "Range : %.3f");
	light->SetRange(AccessValue);
}

void ExposeAmbient(Light* light) {
	XMFLOAT4 colour = light->GetAmbientColour();
	ImGui::ColorEdit4("Ambient", &colour.x);
	light->SetAmbientColour(colour.x, colour.y, colour.z, colour.w);
}

void ExposeColour(Light* light) {
	XMFLOAT4 colour = light->GetDiffuseColour();
	ImGui::ColorEdit4("Diffuse", &colour.x);
	light->SetDiffuseColour(colour.x, colour.y, colour.z, colour.w);

	colour = light->GetSpecularColour();
	ImGui::ColorEdit4("Specular", &colour.x);
	light->SetSpecularColour(colour.x, colour.y, colour.z, colour.w);
	float AccessValue = 0.0f;
	AccessValue = light->GetSpecularPower();
	ImGui::SliderFloat("Specular", &AccessValue, 2.0f, 100.f, "Power : %.3f");
	light->SetSpecularPower(AccessValue);

}

void ChangeType(Light* light) {
	int item = static_cast<int>(light->GetType());
	ImGui::PushItemWidth(160);
	ImGui::Combo("LightType", &item, "Ambient\0Point\0Directional\0Off\0");
	light->SetType(static_cast<LightType>(item));
}

void ImGui::ExposeLight(Light* light) {
	ChangeType(light);
	switch (light->GetType()) {
		case kAmbient:
			ExposeAmbient(light);
			break;
		case kPoint:
			ExposePosition(light);
			ExposeColour(light);
			ExposeAttenuation(light);
			break;
		case kDirectional:
			ExposeDirection(light);
			ExposeColour(light);
			break;
		case kOff:
			break;
		default:
			break;
	}
}