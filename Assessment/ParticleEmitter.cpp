#include "ParticleEmitter.h"

ParticleEmitter::ParticleEmitter(ID3D11Device * device, ID3D11DeviceContext * device_context, WCHAR * texture_filename, ParticleSettings settings)
	:ParticleContainer(device, device_context, texture_filename, settings) {
	is_on_ = true;
	emission_rate_ = 100.0f;
	emission_timer_.SetTimer(1.0f / emission_rate_);
	position_ = XMVectorSet(0, 0, 0, 1.0f);
	num_engine_.seed(1729);
}

ParticleEmitter::~ParticleEmitter() {
	ParticleContainer::~ParticleContainer();
}

void ParticleEmitter::Update(float dt) {
	ParticleContainer::Update(dt);

	emission_timer_.Update(dt);
	if (is_on_) {
		if (emission_timer_.IsFinished()) {
			emission_timer_.Reset();
			EmitParticle();
		}
	}
}

void ParticleEmitter::SetEmissionRate(float rate) {
	emission_rate_ = rate;
	emission_timer_.SetTimer(1.0f / emission_rate_);
}

void ParticleEmitter::SetPosition(float x, float y, float z) {
	position_ = XMVectorSet(x, y, z, 1.0f);
}
void ParticleEmitter::TurnOn() {
	is_on_ = true;
}

void ParticleEmitter::TurnOff() {
	is_on_ = false;
}

bool ParticleEmitter::IsOn() {
	return is_on_;
}

float ParticleEmitter::EmissionRate() {
	return emission_rate_;
}

XMFLOAT3 ParticleEmitter::Position() {
	XMFLOAT3 temp(XMVectorGetX(position_), XMVectorGetY(position_), XMVectorGetZ(position_));
	return temp;
}

void ParticleEmitter::EmitParticle() {
	for (int i = 0; i < settings_.effect_particles; i++) {
		if (num_particles_ < max_particles_) {
			std::uniform_real_distribution<float> v_x(settings_.min_x_velocity, settings_.max_x_velocity);
			std::uniform_real_distribution<float> v_y(settings_.min_y_velocity, settings_.max_y_velocity);
			std::uniform_real_distribution<float> v_z(settings_.min_z_velocity, settings_.max_z_velocity);

			std::uniform_real_distribution<float> size(settings_.min_start_size, settings_.max_start_size);
			std::uniform_real_distribution<float> end_size(settings_.min_end_size, settings_.max_end_size);

			std::uniform_real_distribution<float> duration(settings_.min_duration, settings_.max_duration);


			particles_[num_particles_]->set_position(position_);
			particles_[num_particles_]->set_velocity(v_x(num_engine_), v_y(num_engine_), v_z(num_engine_));
			particles_[num_particles_]->set_size(size(num_engine_));
			particles_[num_particles_]->set_end_size(end_size(num_engine_));
			particles_[num_particles_]->set_duration(duration(num_engine_));


			++num_particles_;
		}
	}
}
