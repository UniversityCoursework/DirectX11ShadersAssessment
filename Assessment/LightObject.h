#ifndef _LIGHT_GAMEOBJECT_H
#define _LIGHT_GAMEOBJECT_H

#include "../DXFramework/Light.h"

#include "GameObject.h"

/// <summary>
/// Light object for controlling and modifing Lights in the editor.
/// </summary>
class LightObject : public GameObject {
public:
	LightObject();
	LightObject(std::string name);
	~LightObject();

	/// <summary>
	/// The light this object represents.
	/// Will be used to determine where to render a Light gizmo if enabled.
	/// Will show all the settings for the Light allowing the light to be modified.
	/// </summary>
	/// <param name="particle_emitter"> New Light. </param>
	void SetLight(Light* light);

	/// <summary>
	/// Sets the position of the light.
	/// Override to set the position of the light, and the light gizmo's mesh.
	/// </summary>
	void SetPosition(float x, float y, float z) override;

	Light* GetLight();

private:
	Light* light_;
};

#endif

