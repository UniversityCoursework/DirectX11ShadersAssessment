#pragma once

#include "../DXFramework/imgui.h"
#include "../DXFramework/d3d.h"

namespace ImGui {

	/// <summary>
	/// Exposes the Wireframe Mode Toggle for use in a menu Item selection. 
	/// </summary>
	/// <param name="d3d"></param>
	void ExposeWireFrameMenuItem(D3D* d3d);
		
	/// <summary>
	/// Exposes the Wireframe Mode As a CheckBox for use in an ImGui Window. 
	/// </summary>
	/// <param name="d3d"></param>
	void ExposeWireFrame(D3D* d3d);

	/// <summary>
	/// Displays the current FPS detected by Imgui.
	/// On the sameline at a set width, for use on the IMGUI top bar.
	/// </summary>
	void DisplayFPS();
		
	/// <summary>
	/// Exposes all available options for screen resolutions on this device.
	/// Runs an expensive generation of all Screen resolutions once on first call.
	/// Only exposes Resolutions available on this device, however it does not 
	/// expose the different refresh rates available.
	/// </summary>
	/// <returns> True if there has been a Resolution Change.</returns>
	bool ExposeResolutionOptions(D3D* d3d, HWND hwnd);
}
