#include "ChromaticAberationProccess.h"

ChromaticProcess::ChromaticProcess()
	:PostProcess() {
	type_ = PostProcessType::kChromaticAberation;
	displacement_scale_ = XMFLOAT3(0.995f, 1.000f, 1.005f);
	base_radius_ = 0.9f;
	falloff_radius_ = 1.8f;
	chroma_power_ = 5.0f;
}

ChromaticProcess::~ChromaticProcess() {
}

void ChromaticProcess::SetDisplacementScale(XMFLOAT3 displacement) {
	displacement_scale_ = displacement;
}

void ChromaticProcess::SetBaseRadius(float base_radius) {
	base_radius_ = base_radius;
}

void ChromaticProcess::SetFalloffRadius(float falloff_radius) {
	falloff_radius_ = falloff_radius;
}

void ChromaticProcess::SetChromaPower(float power) {
	chroma_power_ = power;
}

XMFLOAT3 ChromaticProcess::DisplacementScale() {
	return displacement_scale_;
}

float ChromaticProcess::BaseRadius() {
	return base_radius_;
}

float ChromaticProcess::FalloffRadius() {
	return falloff_radius_;
}

float ChromaticProcess::ChromaPower() {
	return chroma_power_;
}
