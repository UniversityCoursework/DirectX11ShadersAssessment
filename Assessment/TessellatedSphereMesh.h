#ifndef _TessellatedSphereMesh_H_
#define _TessellatedSphereMesh_H_

#include "../DXFramework/BaseMesh.h"

using namespace DirectX;

/// <summary>
/// Generates Vertex/Quad information from CubeSphere with control point patchlists for Tessellation.
/// </summary>
class TessellatedSphereMesh : public BaseMesh {
public:
	TessellatedSphereMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution = 10);
	~TessellatedSphereMesh();

	void SendData(ID3D11DeviceContext* device_context);
protected:
	void InitBuffers(ID3D11Device* device);
	int resolution_;
};

#endif