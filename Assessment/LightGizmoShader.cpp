#include "LightGizmoShader.h"

LightGizmoShader::LightGizmoShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"shaders/light_gizmo_vs.hlsl", L"shaders/light_gizmo_ps.hlsl");
}

LightGizmoShader::~LightGizmoShader() {

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	if (colour_buffer_) {
		colour_buffer_->Release();
		colour_buffer_ = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void LightGizmoShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename) {
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC colourBufferDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the Matrix Buffer.
	// Vertex Shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &matrix_buffer_);

	// Setup colour Buffer.
	// Pixel Shader.
	colourBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	colourBufferDesc.ByteWidth = sizeof(ColourBufferType);
	colourBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	colourBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	colourBufferDesc.MiscFlags = 0;
	colourBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&colourBufferDesc, NULL, &colour_buffer_);

}

void LightGizmoShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, XMFLOAT4 light_colour){

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	ColourBufferType* colourPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;

	// Setup the Matrix Buffer.
	// Vertex Shader.
	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);
	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);


	// Setup colour buffer.
	// Pixel Shader.	
	// Lock the constant buffer so it can be written to.
	device_context->Map(colour_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// get a pointer to the data in the constant buffer
	colourPtr = (ColourBufferType*)mappedResource.pData;
	// Set Values
	colourPtr->colour = light_colour;

	// unlock the buffer
	device_context->Unmap(colour_buffer_, 0);
	bufferNumber = 0;
	// set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(bufferNumber, 1, &colour_buffer_);

}

void LightGizmoShader::Render(ID3D11DeviceContext* device_context, int index_count) {
	// Base render function.
	BaseShader::Render(device_context, index_count);
}



