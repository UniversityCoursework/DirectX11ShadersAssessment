#ifndef _ASSESSMENT_APP_H
#define _ASSESSMENT_APP_H

#include "../DXFramework/baseapplication.h"
#include "../DXFramework/RenderTexture.h"
#include "../DXFramework/spheremesh.h"
#include "../DXFramework/OrthoMesh.h"
#include "D3D.h"

#include "TessellatedSphereMesh.h"

#include "ParticleEmitter.h"

#include "LightGizmoShader.h"
#include "MultiLightShader.h"
#include "WaveShader.h"

#include "ChromaticShader.h"
#include "NoiseShader.h"

#include "PointsToQuadShader.h"

#include "GameObject.h"
#include "WaveObject.h"
#include "LightObject.h"
#include "EmitterObject.h"

#include "PostProcess.h"
#include "ChromaticAberationProccess.h"
#include "NoiseProcess.h"

#include <vector>

/// <summary>
/// Assessment Application:
/// 3rd Year Graphics Programming for shaders using DX11.
/// <para> - Multiple Lights with specular/attenuation etc.</para>
/// <para> - Tesselation of a sphere, with Nvidia wave function passed through vertices.</para>
/// <para> - Post Proccess - Noise, Chromatic aberation.</para>
/// <para> - Geometry shader - particle effects from custom point mesh, fire and smoke trail.</para>
/// </summary>
class AssessmentApp : public BaseApplication {
public:

	AssessmentApp();
	~AssessmentApp();
	void Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input* input);

	bool Frame();

protected:
	bool Render();

private:

	bool show_editor_outliner_;
	bool show_editor_gizmos_;
	bool show_editor_post_proccess_;
	PostProcessType selected_proccess_;
	float game_time_;

	// Meshes

	SphereMesh* sphere_mesh_;
	OrthoMesh* orth_mesh_;
	TessellatedSphereMesh* tess_sphere_mesh_;

	ParticleEmitter* fire_emitter_;
	ParticleEmitter* smoke_emitter_;

	// Shaders

	LightGizmoShader* light_gizmo_shader_;
	WaveShader* wave_shader_;
	ChromaticShader* chromatic_shader_;
	NoiseShader* noise_shader_;
	PointsToQuadShader* points_to_quad_shader_;
	MultiLightShader* light_shader_;


	// array of lights for the light shaders.
	Light* lights_[MAXLIGHTS];

	// Game objects.

	WaveObject* planet_;
	WaveObject* jello_;
	GameObject* floating_orb_;
	EmitterObject* fire_emitter_object_;
	EmitterObject* smoke_emitter_object_;

	// Post Process effect handlers.
	ChromaticProcess* chromatic_editor_process_;
	NoiseProcess* noise_editor_process_;

	// Render target for post processing.
	RenderTexture* scene_render_texture_;

	// Noise texture used in chromatic aberation.
	Texture* noise_texture_;

	// Simple Wrapper for drawing light Widgets
	std::vector<LightObject*> editor_lights_;

	// Simple Wrapper for updating emitters.
	std::vector<ParticleContainer*> particle_containers_;

	// Handles deletion of the post process and used to pass them to the Post process Editor Window.
	std::vector<PostProcess*> post_proccess_effects_;

	// Handles deletion of objects, and used for passing all modifiable objects to outliner.
	std::vector<GameObject*> gameobjects_;

	/// <summary>
	/// Renders the default scene to the render texture.
	/// </summary>
	void RenderSceneToTexture();

	/// <summary>
	/// Modifies the rendertarget using the chromatic aberation shader.
	/// Then redners to a full screen ortho mesh.
	/// </summary>
	void RenderChromaticAberation();

	/// <summary>
	/// Modifies the rendertarget using the noise shader.
	/// Then redners to a full screen ortho mesh.
	/// </summary>
	void RenderNoise();

	/// <summary>
	/// The default scene rendered normally.
	/// </summary>
	void RenderScene();

	/// <summary>
	/// Handles all of the ImGui Ui rendering.
	/// </summary>
	void RenderImGui();

	/// <summary>
	/// Updates the editor Gizmos, used for shwoing position of something that doesnt have a mesh.
	/// Currently just updates the light gzimo's to match their lights.
	/// </summary>
	void UpdateGizmos();

	/// <summary>
	/// Renders the Editor Gizmos.
	/// Currently just renders the light gizmos, as a sphere with colour matched to the diffuse colour
	///  of the light.
	/// </summary>
	void RenderGizmos();
};

#endif