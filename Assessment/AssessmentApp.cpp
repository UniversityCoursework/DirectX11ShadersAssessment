#include "AssessmentApp.h"

#include "../DXFramework/imgui.h"
#include "../DXFramework/imgui_impl_dx11.h"
#include "imgui_style.h"
#include "imgui_d3d.h"

#include "imgui_outliner.h"
#include "imgui_utils.h"
#include "imgui_post_process.h"

#include "FireEffectSettings.h"
#include "VapourTrailSettings.h"

AssessmentApp::AssessmentApp() {
}

void AssessmentApp::Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input* input) {
	// Call super Init function (required!)
	BaseApplication::Init(h_instance, hwnd, screen_width, screen_height, input);
	ImGui::SetupImGuiStyle(true, 0.8f);

	// default values for UI etc.
	show_editor_outliner_ = true;
	show_editor_post_proccess_ = true;
	show_editor_gizmos_ = false;
	game_time_ = 0.0f;
	selected_proccess_ = PostProcessType::kNothing;

	// Setup the meshes etc.
	sphere_mesh_ = new SphereMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/DefaultDiffuse.png");
	orth_mesh_ = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), screen_width, screen_height);
	tess_sphere_mesh_ = new TessellatedSphereMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/DefaultDiffuse.png");

	fire_emitter_ = new ParticleEmitter(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/fire1.png", FireEffectSettings());
	particle_containers_.push_back(fire_emitter_);
	smoke_emitter_ = new ParticleEmitter(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/smoke2.png", VapourTrailSettings());
	particle_containers_.push_back(smoke_emitter_);

	// Setup the shaders
	light_gizmo_shader_ = new LightGizmoShader(m_Direct3D->GetDevice(), hwnd);
	light_shader_ = new MultiLightShader(m_Direct3D->GetDevice(), hwnd);
	wave_shader_ = new WaveShader(m_Direct3D->GetDevice(), hwnd);
	chromatic_shader_ = new ChromaticShader(m_Direct3D->GetDevice(), hwnd);
	noise_shader_ = new NoiseShader(m_Direct3D->GetDevice(), hwnd);
	points_to_quad_shader_ = new PointsToQuadShader(m_Direct3D->GetDevice(), hwnd);

	// Render target for post processing.
	scene_render_texture_ = new RenderTexture(m_Direct3D->GetDevice(), screen_width, screen_height, SCREEN_NEAR, SCREEN_DEPTH);

	// Noise texture used in chromatic aberation.
	noise_texture_ = new Texture(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/perlin_noise.png");

	// Add a blank post proccess.	
	post_proccess_effects_.push_back(new PostProcess());
	// setup and add the rest of the post process.
	chromatic_editor_process_ = new ChromaticProcess();
	post_proccess_effects_.push_back(chromatic_editor_process_);

	noise_editor_process_ = new NoiseProcess();
	post_proccess_effects_.push_back(noise_editor_process_);

	// Generate Gameobjects for all objects in the scene.
	// This allows them to be selected and manipulated in the Outliner, and Inspector in ImGui.

	planet_ = new WaveObject("Planet");
	planet_->SetMesh(tess_sphere_mesh_);
	planet_->SetPosition(0, -105, 0);
	planet_->SetUniformScale(100.f);
	planet_->SetNearClip(2);
	planet_->SetFarClip(50);
	planet_->SetMinTess(12);
	planet_->SetMaxTess(32);
	planet_->SetAmplitude(0.018f);
	planet_->SetSteepness(0.083f);
	planet_->SetWaveLength(0.045f);

	jello_ = new WaveObject("Jello");
	jello_->SetMesh(tess_sphere_mesh_);
	jello_->SetPosition(0, 0, 0);
	jello_->SetUniformScale(1.f);
	jello_->SetNearClip(2);
	jello_->SetFarClip(15);
	jello_->SetMinTess(1);
	jello_->SetMaxTess(32);
	jello_->SetTessAmp(12);

	jello_->SetAmplitude(0.137f);
	jello_->SetSteepness(0.482f);
	jello_->SetWaveLength(0.333f);

	floating_orb_ = new GameObject("Floating Orb");
	floating_orb_->SetMesh(sphere_mesh_);
	floating_orb_->SetPosition(0, 2.0f, 0);
	floating_orb_->SetEulerRotation(45, 0, 0);
	floating_orb_->SetUniformScale(0.5f);

	fire_emitter_object_ = new EmitterObject("Fire Particle Effect");
	fire_emitter_object_->SetEmitter(fire_emitter_);
	fire_emitter_object_->SetPosition(0, 2, 0);
	fire_emitter_object_->SetEmissionRate(60);

	smoke_emitter_object_ = new EmitterObject("Smoke Particle Effect");
	smoke_emitter_object_->SetEmitter(smoke_emitter_);
	smoke_emitter_object_->SetPosition(0, 0, 0);
	smoke_emitter_object_->SetEmissionRate(30);

	// Add all the game objects to the vector for easy passing to the editor window.
	gameobjects_.push_back(planet_);
	gameobjects_.push_back(jello_);
	gameobjects_.push_back(floating_orb_);
	gameobjects_.push_back(fire_emitter_object_);
	gameobjects_.push_back(smoke_emitter_object_);

	// Set up the 3 lights used in the scene, with some intersting values.

	lights_[0] = new Light;
	lights_[0]->SetType(LightType::kPoint);
	lights_[0]->SetDiffuseColour(1.0f, 0.0f, 0.0f, 1.0f);
	lights_[0]->SetSpecularColour(1.0f, 0.0f, 0.0f, 1.0f);
	lights_[0]->SetPosition(4.f, 2.f, 0.0f);
	lights_[0]->SetDirection(0, 0.f, 1.0f);
	lights_[0]->SetAmbientColour(0.1f, 0.1f, 0.1f, 1.0f);
	lights_[0]->SetSpecularPower(20.f);

	lights_[1] = new Light;
	lights_[1]->SetType(LightType::kPoint);
	lights_[1]->SetDiffuseColour(0.0f, 1.0f, 0.0f, 1.0f);
	lights_[1]->SetSpecularColour(0.0f, 1.0f, 0.0f, 1.0f);
	lights_[1]->SetPosition(-4.0f, 2.f, 0.0f);
	lights_[1]->SetDirection(-1.0f, 0.f, 0.0f);
	lights_[1]->SetAmbientColour(0.f, 0.f, 0.f, 1.0f);
	lights_[1]->SetSpecularPower(20.f);

	lights_[2] = new Light;
	lights_[2]->SetType(LightType::kPoint);
	lights_[2]->SetDiffuseColour(0.0f, 0.0f, 1.0f, 1.0f);
	lights_[2]->SetSpecularColour(0.0f, 0.0f, 1.0f, 1.0f);
	lights_[2]->SetPosition(0.f, 2.f, 4.0f);
	lights_[2]->SetDirection(0.0f, 0.f, -1.0f);
	lights_[2]->SetAmbientColour(0.f, 0.f, 0.f, 1.0f);
	lights_[2]->SetSpecularPower(20.f);

	// Create game objects for manipulating the lights in the editor.
	for (int i = 0; i < MAXLIGHTS; i++) {
		// Generate name for lights in outliner.
		std::string name = "Light: " + std::to_string(i);
		// Add all the lights as a gizmo as want them to be rendered a certain way.
		editor_lights_.push_back(new LightObject(name));
		// Give it a mesh aswell.
		editor_lights_.back()->SetMesh(sphere_mesh_);
		editor_lights_.back()->SetLight(lights_[i]);
		// Set the mesh's scale.
		editor_lights_.back()->SetUniformScale(0.1f);
		// add them to scene gameobjects as well ( allows them to show up easily in the outliner).
		gameobjects_.push_back(editor_lights_.back());
	}

}

AssessmentApp::~AssessmentApp() {
	// Run base application deconstructor
	BaseApplication::~BaseApplication();

	for each (ParticleContainer* emitter in particle_containers_) {
		if (emitter) {
			delete emitter;
			emitter = nullptr;
		}
	}

	if (sphere_mesh_) {
		delete sphere_mesh_;
		sphere_mesh_ = nullptr;
	}

	if (orth_mesh_) {
		delete orth_mesh_;
		orth_mesh_ = nullptr;
	}

	if (tess_sphere_mesh_) {
		delete tess_sphere_mesh_;
		tess_sphere_mesh_ = nullptr;
	}

	if (light_gizmo_shader_) {
		delete light_gizmo_shader_;
		light_gizmo_shader_ = nullptr;
	}

	if (wave_shader_) {
		delete wave_shader_;
		wave_shader_ = nullptr;
	}

	if (chromatic_shader_) {
		delete chromatic_shader_;
		chromatic_shader_ = nullptr;
	}
	if (noise_shader_) {
		delete noise_shader_;
		noise_shader_ = nullptr;
	}

	if (points_to_quad_shader_) {
		delete points_to_quad_shader_;
		points_to_quad_shader_ = nullptr;
	}

	if (light_shader_) {
		delete light_shader_;
		light_shader_ = nullptr;
	}

	for (int i = 0; i < MAXLIGHTS; i++) {
		if (lights_[i]) {
			delete lights_[i];
			lights_[i] = nullptr;
		}
	}

	for each (GameObject* gameobject in gameobjects_) {
		if (gameobject) {
			delete gameobject;
			gameobject = nullptr;
		}
	}

	for each (PostProcess* post_process in post_proccess_effects_) {
		if (post_process) {
			delete post_process;
			post_process = nullptr;
		}
	}

	if (scene_render_texture_) {
		delete scene_render_texture_;
		scene_render_texture_ = nullptr;
	}

	if (noise_texture_) {
		delete noise_texture_;
		noise_texture_ = nullptr;
	}
}

bool AssessmentApp::Frame() {
	bool result;

	result = BaseApplication::Frame();
	if (!result) {
		return false;
	}

	// Render the graphics.
	result = Render();

	if (!result) {
		return false;
	}

	return true;
}

bool AssessmentApp::Render() {

	game_time_ += m_Timer->GetTime();

	// Clear the scene. (Light Grey Colour)
	m_Direct3D->BeginScene(0.1f, 0.1f, 0.1f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();
	UpdateGizmos();
	// Update all the particle containers
	for each (ParticleContainer* emitter in particle_containers_) {
		emitter->Update(m_Timer->GetTime());
		emitter->BuildBuffers(m_Direct3D->GetDevice());
	}

	// Save wireframe mode
	bool oldWireFrame = m_Direct3D->IsWireFrameModeOn();

	// Render Post Processing technique
	switch (selected_proccess_) {
		case PostProcessType::kNothing:
			RenderScene();
			break;
		case PostProcessType::kChromaticAberation:
			RenderSceneToTexture();
			// Turn off as rendering ortho texture. ( only if its on).
			if (oldWireFrame) {
				m_Direct3D->TurnOffWireframe();
			}
			RenderChromaticAberation();
			break;
		case PostProcessType::kNoise:
			RenderSceneToTexture();
			// Turn off as rendering ortho texture. ( only if its on).
			if (oldWireFrame) {
				m_Direct3D->TurnOffWireframe();
			}
			RenderNoise();
			break;
	}
	// Reset wireframe.
	if (oldWireFrame) {
		m_Direct3D->TurnOnWireframe();
	}

	
	
	RenderImGui();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();
	return true;
}

void AssessmentApp::RenderSceneToTexture() {
	// Set Render target to the scene texture.
	scene_render_texture_->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the Render texture. (Light Grey Colour)
	scene_render_texture_->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.1f, 0.1f, 0.1f, 1.0f);
	RenderScene();
	// Reset Render target to the output.
	m_Direct3D->SetBackBufferRenderTarget();
}

void AssessmentApp::RenderChromaticAberation() {
	// Generate Matrices for Ortho Projection.
	XMMATRIX worldMatrix, baseViewMatrix, orthoMatrix;
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetBaseViewMatrix(baseViewMatrix);

	// Turn depth off as ortho.
	m_Direct3D->TurnZBufferOff();

	// Send geometry data.
	orth_mesh_->SendData(m_Direct3D->GetDeviceContext());

	// Pass the scene, the noise texture, and the chromatic aberation settings from the editor.
	chromatic_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, baseViewMatrix, orthoMatrix,
		scene_render_texture_->GetShaderResourceView(),
		chromatic_editor_process_->DisplacementScale(), chromatic_editor_process_->BaseRadius(), chromatic_editor_process_->FalloffRadius(),
		chromatic_editor_process_->ChromaPower());

	// Render it onto the Ortho mesh.
	chromatic_shader_->Render(m_Direct3D->GetDeviceContext(), orth_mesh_->GetIndexCount());

	// Turn depth back on.
	m_Direct3D->TurnZBufferOn();
}

void AssessmentApp::RenderNoise() {
	// Generate Matrices for Ortho Projection.	
	XMMATRIX worldMatrix, baseViewMatrix, orthoMatrix;
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetBaseViewMatrix(baseViewMatrix);

	// Turn depth off as ortho.
	m_Direct3D->TurnZBufferOff();

	// Send geometry data.
	orth_mesh_->SendData(m_Direct3D->GetDeviceContext());

	// Pass the scene, and the noise settings from the process editor.
	noise_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), worldMatrix, baseViewMatrix, orthoMatrix,
		scene_render_texture_->GetShaderResourceView(),
		(int)noise_editor_process_->NoiseType(), noise_editor_process_->NoiseLevel());

	// Render it onto the Ortho mesh.
	noise_shader_->Render(m_Direct3D->GetDeviceContext(), orth_mesh_->GetIndexCount());

	// Turn depth back on.
	m_Direct3D->TurnZBufferOn();
}

void AssessmentApp::RenderScene() {
	// Generate the view and projection Matrices, world matrix is taken from the Game object.
	XMMATRIX viewMatrix, projectionMatrix;
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);

	// Send Floating Orb geometry data.
	floating_orb_->Mesh()->SendData(m_Direct3D->GetDeviceContext());
	// Pass in the floating orb; world matrix (pos,rotation,scale), and texture. As well as all the lights and the camera position.
	light_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), floating_orb_->WorldMatrix(), viewMatrix, projectionMatrix, floating_orb_->Mesh()->GetTexture(),
		lights_, m_Camera->GetPosition());
	light_shader_->Render(m_Direct3D->GetDeviceContext(), floating_orb_->Mesh()->GetIndexCount());

	// Send Planet geometry data.
	planet_->Mesh()->SendData(m_Direct3D->GetDeviceContext());
	// Pass in the Planet; world matrix (pos,rotation,scale), and texture. As well as all the lights and the camera position.
	// And the waves parameters as well as the current game time, used to generate the waves.
	wave_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), planet_->WorldMatrix(), viewMatrix, projectionMatrix, planet_->Mesh()->GetTexture(),
		planet_->WaveParams(), lights_, m_Camera->GetPosition(), game_time_);
	wave_shader_->Render(m_Direct3D->GetDeviceContext(), planet_->Mesh()->GetIndexCount());

	// Send Jello geometry data.
	jello_->Mesh()->SendData(m_Direct3D->GetDeviceContext());
	// Pass in the Jello; world matrix (pos,rotation,scale), and texture. As well as all the lights and the camera position.
	// And the waves parameters as well as the current game time, used to generate the waves.
	wave_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), jello_->WorldMatrix(), viewMatrix, projectionMatrix, jello_->Mesh()->GetTexture(),
		jello_->WaveParams(), lights_, m_Camera->GetPosition(), game_time_);
	wave_shader_->Render(m_Direct3D->GetDeviceContext(), jello_->Mesh()->GetIndexCount());


	// Use additive blending for the particles, requires no depth sorting so faster and easier.
	m_Direct3D->TurnOnAdditiveBlending();
	// Turn off writing to the depth buffer, but still use it for testing.
	// Stops particles from rendering ontop of everything, but allows them to render ontop of each other.
	m_Direct3D->TurnZBufferWriteOff();

	// Send Fire geometry data.
	fire_emitter_object_->Mesh()->SendData(m_Direct3D->GetDeviceContext());

	points_to_quad_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), fire_emitter_object_->WorldMatrix(), viewMatrix, projectionMatrix, fire_emitter_object_->Mesh()->GetTexture(), fire_emitter_object_->IsOffsetByUp());
	points_to_quad_shader_->Render(m_Direct3D->GetDeviceContext(), fire_emitter_object_->Mesh()->GetIndexCount());

	// Send Smoke geometry data.
	smoke_emitter_object_->Mesh()->SendData(m_Direct3D->GetDeviceContext());

	points_to_quad_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), smoke_emitter_object_->WorldMatrix(), viewMatrix, projectionMatrix, smoke_emitter_object_->Mesh()->GetTexture(), smoke_emitter_object_->IsOffsetByUp());
	points_to_quad_shader_->Render(m_Direct3D->GetDeviceContext(), smoke_emitter_object_->Mesh()->GetIndexCount());

	// Make sure we reset the blending state so as not to affect anything else.
	m_Direct3D->TurnOffBlending();
	// Turn back on the writing to the depth buffer, so that objects are rendered in the correct order.
	m_Direct3D->TurnZBufferWriteOn();

	if (show_editor_gizmos_) {
		RenderGizmos();
	}
}

void AssessmentApp::RenderImGui() {
	// Ensure all shaders are reset to null before rendering ImGui.
	m_Direct3D->ResetShadersNull();

	// Pass in screen resolution so mouse input can be scaled to resolution if in fullscreen at low resolution.
	ImGui_ImplDX11_NewFrameDPI((int)m_Direct3D->Resolution().x, (int)m_Direct3D->Resolution().y);
	{
		// Menu bar allong the top of the screen.
		if (ImGui::BeginMainMenuBar()) {
			// Drop down menu for exposing editor settings.
			if (ImGui::BeginMenu("Menu")) {
				ImGui::ExposeWireFrameMenuItem(m_Direct3D);
				ImGui::MenuItem("Editor Gizmos", NULL, &show_editor_gizmos_);
				ImGui::MenuItem("Post Proccess Editor", NULL, &show_editor_post_proccess_);
				ImGui::MenuItem("Editor", NULL, &show_editor_outliner_);
				if (ImGui::MenuItem("Reset Camera", NULL)) {
					m_Camera->SetPosition(0, 0, -10.0f);
					m_Camera->SetRotation(0, 0, 0);
				}
				ImGui::EndMenu();
			}

			// Shows the Resolution changer options, and windowed fullscreen options.
			// If it returns true it means the screen has changed size.
			if (ImGui::ExposeResolutionOptions(m_Direct3D, wnd)) {
				// The window has been resized, therefore we Resize the Render Targets and ortho meshes.
				// ImGui handles its own resizing.
				int new_width = (int)m_Direct3D->Resolution().x;
				int new_height = (int)m_Direct3D->Resolution().y;

				orth_mesh_->ResizeMesh(m_Direct3D->GetDevice(), new_width, new_height);
				scene_render_texture_->ResizeTexture(m_Direct3D->GetDevice(), new_width, new_height);
			}
			// Displays the current fps at the end of the 
			ImGui::DisplayFPS();
			ImGui::EndMainMenuBar();
		}

		if (show_editor_outliner_) {
			ImGui::EditorOutliner(&show_editor_outliner_, gameobjects_);
		}

		if (show_editor_post_proccess_) {
			ImGui::PostProcessEditorWindow(&show_editor_post_proccess_, post_proccess_effects_, selected_proccess_);
		}
	}
	ImGui::Render();
}

void AssessmentApp::UpdateGizmos() {
	for (int i = 0; i < MAXLIGHTS; i++) {
		editor_lights_[i]->SetPosition(lights_[i]->GetPosition().x, lights_[i]->GetPosition().y, lights_[i]->GetPosition().z);
	}
}

void AssessmentApp::RenderGizmos() {
	XMMATRIX viewMatrix, projectionMatrix;
	// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.	
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);

	for (int i = 0; i < MAXLIGHTS; i++) {
		// Send mesh data.
		sphere_mesh_->SendData(m_Direct3D->GetDeviceContext());
		// Set shader parameters; matrices, texture, and Lights diffuse colour.
		// Could make this colour selection dynamic based on light type.
		light_gizmo_shader_->SetShaderParameters(m_Direct3D->GetDeviceContext(), editor_lights_[i]->WorldMatrix(), viewMatrix, projectionMatrix, lights_[i]->GetDiffuseColour());
		light_gizmo_shader_->Render(m_Direct3D->GetDeviceContext(), sphere_mesh_->GetIndexCount());
	}
}


