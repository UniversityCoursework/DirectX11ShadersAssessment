#ifndef _NOISE_SHADER_H
#define _NOISE_SHADER_H

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Post Proccessing effect of adding noise to scene.
/// </summary>
class NoiseShader : public BaseShader {
private:
	struct NoiseBufferType {
		XMINT4 type;
		float noiseLevel;
		XMFLOAT3 padding;
	};

public:

	NoiseShader(ID3D11Device* device, HWND hwnd);
	~NoiseShader();

	void SetShaderParameters(ID3D11DeviceContext* device_context,
		const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* scene,
		int type_of_noise, float noise_level);

	void Render(ID3D11DeviceContext* device_context, int index_count);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11Buffer* noise_buffer_;

	ID3D11SamplerState* sample_state_;
};

#endif
