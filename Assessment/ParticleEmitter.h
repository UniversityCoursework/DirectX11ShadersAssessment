#ifndef _PARTICLE_EMITTER_H_
#define _PARTICLE_EMITTER_H_

#include "ParticleContainer.h"
#include "SimpleTimer.h"

#include <random>

using namespace DirectX;

/// <summary>
/// Emits particles at the given rate.
/// Using the particle settings provided to generate new particle going in a random direction etc.
/// </summary>
class ParticleEmitter : public ParticleContainer {
public:
	///<summary> Constructs the Particle container, Using settings.max_particles for the array of particles. </summary>
	///<param name = "settings"> Can be any derived class, allowing for different effects from one container. </param>
	ParticleEmitter(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, ParticleSettings settings);
	~ParticleEmitter();

	
	/// <summary>
	/// Handles generation of new particle based on emission rate, with a basic inaccurate timer.
	/// Overriden update of ParticleContainer still cause Parent update to handle deletion/culling etc.
	/// </summary>	
	void Update(float dt) override;

	/// <summary>
	/// How many emits per second. (limited by refresh rate etc.)
	/// </summary>
	/// <param name="rate"> Number of particles in a second.</param>
	void SetEmissionRate(float rate);
	
	/// <summary>
	/// Set the position to emit said particles at.
	/// </summary>
	void SetPosition(float x, float y, float z);

	/// <summary>
	/// Turns the emitter on, causing it to start emitting particles.
	/// </summary>
	void TurnOn();
	/// <summary>
	/// Turns the emitter off, stopping it from emitting more particles. Currently emitted particles will continue to render until they die.
	/// </summary>
	void TurnOff();

	bool IsOn();
	float EmissionRate();
	XMFLOAT3 Position();

protected:
	void EmitParticle();

	bool is_on_;
	float emission_rate_;
	SimpleTimer emission_timer_;

	XMVECTOR position_;

	// random number engine used to generate accurate random number generation for particle emission.
	std::mt19937 num_engine_;

};

#endif
