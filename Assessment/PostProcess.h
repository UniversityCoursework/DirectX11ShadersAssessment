#ifndef _POST_PROCESS_H
#define _POST_PROCESS_H

#include <string>

/// <summary>
/// Available types of PostProcesses.
/// </summary>
enum PostProcessType {
	kNothing,
	kChromaticAberation,
	kNoise
};

/// <summary>
/// Simple Utility function to ouput Enum in ImGui.
/// </summary>
/// <param name="type"> Type to be converted into a string. </param>
/// <returns> PostProcessType formated as a string. Useable for ImGui etc.</returns>
static std::string PostProcessTypeToString(PostProcessType type) {
	switch (type) {
		case kNothing:
			return "Nothing";
			break;
		case kChromaticAberation:
			return "Chromatic Aberation";
			break;
		case kNoise:
			return "Noise";
			break;
		default:
			return "Corrupt Data";
			break;
	}
}

/// <summary>
///	Holds proccess type, for use with Proccess Editor.
/// Base class for any interactable/modifiable procces in the scene.
/// </summary>
class PostProcess {
public:
	PostProcess() {
		type_ = kNothing;
	}

	virtual ~PostProcess() {};
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p) {
		_mm_free(p);
	}
	PostProcessType Type() { return type_; }
protected:
	PostProcessType type_;
};

#endif
