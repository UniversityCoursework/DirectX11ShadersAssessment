#ifndef _CHROMATIC_SHADER_H
#define _CHROMATIC_SHADER_H

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Post Proccessing effect of offseting RGB values for Chromatic Aberations.
/// Varying based on distance from centre of screen.
/// </summary>
class ChromaticShader : public BaseShader {
private:
	struct ChromaticAberationBufferType {
		XMFLOAT3 displacementScale;
		float baseRadius;
		float falloffRadius;
		float chromaPower;
		XMFLOAT2 padding;
	};

public:

	ChromaticShader(ID3D11Device* device, HWND hwnd);
	~ChromaticShader();

	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* scene, XMFLOAT3 displacement_scale, float base_radius, float falloff_radius, float chroma_power);

	void Render(ID3D11DeviceContext* device_context, int index_count);

private:
	void InitShader(WCHAR* vsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrix_buffer_;
	ID3D11Buffer* chromatic_buffer_;

	ID3D11SamplerState* sample_state_;
};

#endif
