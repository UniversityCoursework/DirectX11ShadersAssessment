#ifndef _CHROMATIC_PROCESS_H
#define _CHROMATIC_PROCESS_H

#include "PostProcess.h"

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

/// <summary>
/// Shifts the RGB values by an RGB offset.
/// Causes a distortion effect with rgb channels rendering in different locations.
/// Offset based on distance from the centre of the screen.
/// </summary>
class ChromaticProcess : public PostProcess {
public:
	ChromaticProcess();
	~ChromaticProcess();

	/// <summary>
	/// Sets the amount of offseting in the RGB channels.
	/// </summary>	
	void SetDisplacementScale(XMFLOAT3 displacement);

	/// <summary>
	/// Sets the lower bound radius used for lerping the distance.
	/// </summary>	
	void SetBaseRadius(float base_radius);

	/// <summary>
	/// Sets the max bound radius used for lerping the distance.
	/// </summary>	
	void SetFalloffRadius(float falloff_radius);

	/// <summary>
	/// Set the strength of chromatic aberation effect.
	/// </summary>	
	void SetChromaPower(float power);

	XMFLOAT3 DisplacementScale();
	float BaseRadius();
	float FalloffRadius();
	float ChromaPower();

protected:
	XMFLOAT3 displacement_scale_;
	float base_radius_;
	float falloff_radius_;
	float chroma_power_;

};

#endif
