#include "imgui_gameobjects.h"

#include "imgui_lighting.h"

#include "WaveObject.h"
#include "LightObject.h"
#include "EmitterObject.h"

void ExposePosition(GameObject & gameobject) {
	XMFLOAT3 position = gameobject.Position();
	ImGui::DragFloat3("Position", &position.x, 0.1f);
	gameobject.SetPosition(position.x, position.y, position.z);
}

void ExposeScale(GameObject & gameobject) {
	XMFLOAT3 scale = gameobject.Scale();
	ImGui::DragFloat3("Scale", &scale.x, 0.1f);
	gameobject.SetScale(scale.x, scale.y, scale.z);

	if (ImGui::SmallButton("Set Scale to X")) {
		gameobject.SetScale(scale.x, scale.x, scale.x);
	}
}

void ExposeEulerRotation(GameObject & gameobject) {
	XMFLOAT3 rotation = gameobject.EularRotation();
	ImGui::SliderFloat3("Rotation", &rotation.x, -180.0f, 180.0f, "%.0f");
	gameobject.SetEulerRotation(rotation.x, rotation.y, rotation.z);
}

void ExposeWaveObject(WaveObject& waveobject) {
	ImGui::Text("Tesselation Parameters");

	ImGui::DragFloatRange2("Clip", &waveobject.WaveParams()->nearClip, &waveobject.WaveParams()->farClip, 1.0f, 0.0f, 200.0f, "Near: %.0f", "Far: %.0f");
	ImGui::DragFloatRange2("Tesselation", &waveobject.WaveParams()->minTess, &waveobject.WaveParams()->maxTess, 1.0f, 1.0f, 64, "Min: %.0f", "Max: %.0f");
	ImGui::SliderFloat("Tess Power", &waveobject.WaveParams()->tessAmp, 1.0f, 64.f, "%.0f");

	ImGui::Text("Wave Parameters");
	ImGui::SliderFloat("Amplitude", &waveobject.WaveParams()->amplitude, 0.001f, 0.5f, "%.3f");
	ImGui::SliderFloat("Steepness", &waveobject.WaveParams()->steepness, 0.001f, 0.5f, "%.3f");
	ImGui::SliderFloat("Wave Length", &waveobject.WaveParams()->waveLength, 0.001f, 0.5f, "%.3f");
}

void ExposeEmitterObject(EmitterObject& emitterObject) {
	ImGui::Text("Emitter Settings:");

	bool emitterOn = emitterObject.IsOn();
	ImGui::SameLine();
	if (emitterOn) {
		ImGui::Checkbox("On", &emitterOn);
	} else {
		ImGui::Checkbox("Off", &emitterOn);
	}
	if (emitterOn) {
		emitterObject.TurnOn();
	} else {
		emitterObject.TurnOff();
	}

	bool isOffsetUp = emitterObject.IsOffsetByUp();
	ImGui::TextWrapped("Decide if the particle is offset to match camera up, or just set to vertical up (0,1,0). Best to be above to notice change.");

	ImGui::Checkbox("Off Set Up", &isOffsetUp);
	emitterObject.SetOffsetByUp(isOffsetUp);

	ImGui::Text("Num: %d / %d", emitterObject.NumOfParticles(), emitterObject.MaxNumOfParticles());

	float emissionRate = emitterObject.EmissionRate();
	ImGui::SliderFloat("Rate", &emissionRate, 1.0f, 60.0f, "%.0f", 1.0f);
	emitterObject.SetEmissionRate(emissionRate);

	ImGui::DragFloatRange2("Duration", &emitterObject.EmitterParticleSettings()->min_duration, &emitterObject.EmitterParticleSettings()->max_duration, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");
	ImGui::DragFloatRange2("Velocity X", &emitterObject.EmitterParticleSettings()->min_x_velocity, &emitterObject.EmitterParticleSettings()->max_x_velocity, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");
	ImGui::DragFloatRange2("Velocity Y", &emitterObject.EmitterParticleSettings()->min_y_velocity, &emitterObject.EmitterParticleSettings()->max_y_velocity, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");
	ImGui::DragFloatRange2("Velocity Z", &emitterObject.EmitterParticleSettings()->min_z_velocity, &emitterObject.EmitterParticleSettings()->max_z_velocity, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");
	ImGui::DragFloatRange2("Start Size", &emitterObject.EmitterParticleSettings()->min_start_size, &emitterObject.EmitterParticleSettings()->max_start_size, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");
	ImGui::DragFloatRange2("End Size", &emitterObject.EmitterParticleSettings()->min_end_size, &emitterObject.EmitterParticleSettings()->max_end_size, 0.1f, 0.0f, 0.0f, "Min: %.1f", "Max: %.1f");


	static float gravity[3] = {
		emitterObject.EmitterParticleSettings()->gravity_x,
		emitterObject.EmitterParticleSettings()->gravity_y,
		emitterObject.EmitterParticleSettings()->gravity_z };
	ImGui::DragFloat3("Gravity", gravity, 0.001f);
	emitterObject.EmitterParticleSettings()->gravity_x = gravity[0];
	emitterObject.EmitterParticleSettings()->gravity_y = gravity[1];
	emitterObject.EmitterParticleSettings()->gravity_z = gravity[2];
}

void ImGui::ExposeGameObject(GameObject& gameobject) {

	// Find out what options to expose based on its type.
	switch (gameobject.Type()) {
		case GameObject::kWave:
		{
			ExposePosition(gameobject);
			ExposeScale(gameobject);
			ExposeEulerRotation(gameobject);
			WaveObject& waveObject = dynamic_cast<WaveObject&>(gameobject);
			ExposeWaveObject(waveObject);

			break;
		}
		case GameObject::kLight:
		{
			LightObject& lightObject = dynamic_cast<LightObject&>(gameobject);
			ExposeLight(lightObject.GetLight());
			break;
		}
		case GameObject::kEmitter:
		{
			ExposePosition(gameobject);
			EmitterObject& emitterObject = dynamic_cast<EmitterObject&>(gameobject);
			ExposeEmitterObject(emitterObject);
			break;
		}
		default:
			ExposePosition(gameobject);
			ExposeScale(gameobject);
			ExposeEulerRotation(gameobject);
			break;
	}

}

void ImGui::GameObjectEditorWindow(GameObject& gameobject) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(300, io.DisplaySize.y / 2.0f - 20);
	ImVec2 window_pos = ImVec2(io.DisplaySize.x - window_size.x, 20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);


	ImGui::Begin("Inspector", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders);

	ExposeGameObject(gameobject);

	ImGui::End();
}