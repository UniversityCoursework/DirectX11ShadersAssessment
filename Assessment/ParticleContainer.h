#ifndef _PARTICLE_CONTAINER_H_
#define _PARTICLE_CONTAINER_H_

#include <vector>

#include "../DXFramework/BaseMesh.h"

#include "Particle.h"
#include "ParticleSettings.h"

using namespace DirectX;

/// <summary>
/// Base particle containment class.
/// All other effects, emitters explosions etc, will be derived from this class 
/// by overriding update and expanding on the classses functionality.
/// </summary>
class ParticleContainer : public BaseMesh {
public:
	///<summary> Constructs the Particle container, Using settings.max_particles for the array of particles. </summary>
	///<param name = "settings"> Can be any derived class, allowing for different effects from one container. </param>
	ParticleContainer(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, ParticleSettings settings);
	virtual ~ParticleContainer();

	// Overloads to insure class is 16 bit aligned for DX varaibles
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}
	void operator delete(void* p) {
		_mm_free(p);
	}

	/// <summary>
	/// Updates all the particles connected to this container, culling dead particles and moving live particles etc.
	/// </summary>	
	virtual void Update(float dt);

	/// <summary>
	/// Send the current generated point list of all living particles to be rendered to the GPU.
	/// </summary>
	void SendData(ID3D11DeviceContext* device_context);

	/// <summary>
	/// Converts all the Living particles stored in the particles vector into a point mesh to be passed to the gpu for rendering.
	/// </summary>	
	void BuildBuffers(ID3D11Device* device);

	/// <summary>
	/// Basic function to prove is working, spawns a particle at the given point, with max particle settins applied.
	/// </summary>	
	void AddParticle(XMFLOAT3 position);

	int NumOfParticles();
	int MaxNumOfParticles();

	ParticleSettings* ContainerParticleSettings();
protected:
	void InitBuffers(ID3D11Device* device) override;

	virtual void UpdateParticle(float dt, int particle);
	struct ParticlePoint {
		XMFLOAT3 position;
		// x - size.
		XMFLOAT2 size;
	};

	ParticleSettings settings_;

	int num_particles_;
	const int max_particles_;

	std::vector<Particle*> particles_;
};

#endif