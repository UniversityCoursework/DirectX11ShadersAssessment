#pragma once
#include "../DXFramework/imgui.h"

#include <vector>
#include <string>

namespace ImGui {
	// Function call back for ImGui's Combo button (enum drop down), takes the data from std::vector std::string 
	// after it has been converted into a void* and casts it back to an std::string* array, then returns the char* style string to the 
	// ImGui function. messy but no other way of doing it with ImGui as wanted a dynamic list for available resolutions.
	static auto vector_getter = [](void* vec, int idx, const char** out_text) {
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*out_text = vector.at(idx).c_str();
		return true;
	};

	static bool Combo(const char* label, int* currIndex, std::vector<std::string>& values) {
		if (values.empty()) { return false; }
		return Combo(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size());
	}

	static bool ListBox(const char* label, int* currIndex, std::vector<std::string>& values) {
		if (values.empty()) { return false; }
		return ListBox(label, currIndex, vector_getter,
			static_cast<void*>(&values), values.size());
	}

}
