#ifndef _PARTICLE_SETTINGS_H
#define _PARTICLE_SETTINGS_H

/// <summary>
/// Base class to be derived from to provide basic set of particle settings.
/// </summary>
class ParticleSettings {
public:
	///<summary> Initializes with default settings. </summary>
	ParticleSettings()
		:max_particles(3000) {
		max_duration = 10.f;
		min_duration = 0.5f;

		min_x_velocity = 0.f;
		max_x_velocity = 0.f;
		min_y_velocity = 0.f;
		max_y_velocity = 1.f;
		min_z_velocity = 0.f;
		max_z_velocity = 0.f;

		gravity_x = 0.0f;
		gravity_y = 0.f;
		gravity_z = 0.0f;

		min_start_size = 1.f;
		max_start_size = 1.f;
		min_end_size = 1.f;
		max_end_size = 1.f;

		effect_particles = 10;
	};
	// duration		
	float min_duration;
	float max_duration;

	// velocity	
	float end_velocity;

	float min_x_velocity;
	float max_x_velocity;

	float min_y_velocity;
	float max_y_velocity;

	float min_z_velocity;
	float max_z_velocity;

	// gravity
	float gravity_x;
	float gravity_y;
	float gravity_z;
	// size
	float min_start_size;
	float max_start_size;

	float min_end_size;
	float max_end_size;

	// particles
	int effect_particles;
	int max_particles;

	///<summary> Overides the copy operator, to allow proper copies. </summary>
	ParticleSettings& operator=(const ParticleSettings& rhs) {
		min_duration = rhs.min_duration;
		max_duration = rhs.max_duration;

		min_x_velocity = rhs.min_x_velocity;
		max_x_velocity = rhs.max_x_velocity;
		min_y_velocity = rhs.min_y_velocity;
		max_y_velocity = rhs.max_y_velocity;
		min_z_velocity = rhs.min_z_velocity;
		max_z_velocity = rhs.max_z_velocity;

		gravity_x = rhs.gravity_x;
		gravity_y = rhs.gravity_y;
		gravity_z = rhs.gravity_z;

		min_start_size = rhs.min_start_size;
		max_start_size = rhs.max_start_size;
		min_end_size = rhs.min_end_size;
		max_end_size = rhs.max_end_size;

		effect_particles = rhs.effect_particles;
		max_particles = rhs.max_particles;
		return *this;
	}


};
#endif

