#ifndef _IMGUI_POSTPROCCESS_H
#define _IMGUI_POSTPROCCESS_H

#include "../DXFramework/imgui.h"

#include "PostProcess.h"

#include <vector>

namespace ImGui {
	
	/// <summary>
	/// Exposes the PostProccess to the current Window, allowing it to be edited at run time. 
	/// </summary>
	/// <param name="post_process"> The post proccess to expose. </param>
	void ExposePostProcess(PostProcess& post_process);

	/// <summary>
	/// Shows the PostProccess editor window. 
	/// </summary>
	/// <param name="is_open"> whether the window is displayed or not. </param>
	/// <param name="post_process_effects"> The list of possible post proccess, is used to generate a drop down list from. </param>
	/// <param name="selected_process"> The currently selected post process, this will be updated if the post process changes. </param>
	void PostProcessEditorWindow(bool* is_open, std::vector<PostProcess*> post_process_effects, PostProcessType &selected_process);
}


#endif
