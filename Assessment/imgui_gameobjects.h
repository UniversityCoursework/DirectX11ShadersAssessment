#ifndef _IMGUI_GAMEOBJECT_H
#define _IMGUI_GAMEOBJECT_H

#include "../DXFramework/imgui.h"

#include "GameObject.h"

#include <vector>

namespace ImGui {
	
	/// <summary>
	/// Exposes the GameObject to the current Window, allowing it to be edited at run time. 
	/// </summary>
	/// <param name="gameobject"> The game object to be edited. </param>
	void ExposeGameObject(GameObject& gameobject);


	/// <summary>
	/// Shows the inspector window with the game objects details show for editing.
	/// </summary>
	/// <param name="gameobject"> The Game object to show up in the inspector. </param>
	void GameObjectEditorWindow(GameObject& gameobject);
}


#endif
