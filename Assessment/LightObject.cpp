#include "LightObject.h"
#include <assert.h>
LightObject::LightObject()
	: GameObject() {
	type_ = kLight;
}

LightObject::LightObject(std::string name)
	: LightObject() {
	name_ = name;
}

LightObject::~LightObject() {
	mesh_ = nullptr;
	light_ = nullptr;
}

void LightObject::SetLight(Light * light) {
	light_ = light;
}

Light * LightObject::GetLight() {
	assert(light_);
	if (light_) {
		return light_;
	}
	return nullptr;
}

void LightObject::SetPosition(float x, float y, float z) {
	position_ = XMVectorSet(x, y, z, 1.0f);
	assert(light_);
	if (light_) {
		light_->SetPosition(x, y, z);
	}
}
