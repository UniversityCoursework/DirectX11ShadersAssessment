#pragma once
#include "../DXFramework/imgui.h"
#include "../DXFramework/Light.h"

namespace ImGui {
	
	/// <summary>
	/// Exposes the light to the current Window, allowing it to be edited at run time. 
	/// </summary>
	/// <param name="light"> The light to be exposed for editing. </param>
	void ExposeLight(Light* light);
}