#include "PointsToQuadShader.h"

PointsToQuadShader::PointsToQuadShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"shaders/point_to_quad_vs.hlsl", L"shaders/point_to_quad_gs.hlsl", L"shaders/point_to_quad_ps.hlsl");
}

PointsToQuadShader::~PointsToQuadShader() {
	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	// Release the matrix constant buffer.
	if (point_to_quad_buffer_) {
		point_to_quad_buffer_->Release();
		point_to_quad_buffer_ = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void PointsToQuadShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename) {
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC quadToPointDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	m_device->CreateBuffer(&matrixBufferDesc, NULL, &matrix_buffer_);

	// Setup point to quad buffer.
	// Pixel Shader, Geometry Shader.
	quadToPointDesc.Usage = D3D11_USAGE_DYNAMIC;
	quadToPointDesc.ByteWidth = sizeof(PointToQuadType);
	quadToPointDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	quadToPointDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	quadToPointDesc.MiscFlags = 0;
	quadToPointDesc.StructureByteStride = 0;

	// Create the constant buffer pointer.
	m_device->CreateBuffer(&quadToPointDesc, NULL, &point_to_quad_buffer_);

	// Create a texture sampler state description.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	m_device->CreateSamplerState(&samplerDesc, &sample_state_);

}

void PointsToQuadShader::InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename) {
	// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
	InitShader(vsFilename, psFilename);

	// Load other required shaders.
	loadGeometryShader(gsFilename);
}

void PointsToQuadShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
	ID3D11ShaderResourceView* texture, bool is_offset_by_up) {
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	PointToQuadType* pointToQuadPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;


	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;
	dataPtr->view = tview;
	dataPtr->projection = tproj;

	// Unlock the constant buffer.
	deviceContext->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Now set the constant buffer in the geometry shader with the updated values.
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);

	// Additional
	// Pixel Shader, Geometry Shader.
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(point_to_quad_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	pointToQuadPtr = (PointToQuadType*)mappedResource.pData;
	pointToQuadPtr->colour = XMFLOAT4(0, 0, 0, 0);
	pointToQuadPtr->type = XMINT4((int)is_offset_by_up, true, 0, 0);

	// Unlock the constant buffer.
	deviceContext->Unmap(point_to_quad_buffer_, 0);

	bufferNumber = 0;
	// Now set the constant buffer. PS
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &point_to_quad_buffer_);

	bufferNumber = 1;
	// Now set the constant buffer. GS
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &point_to_quad_buffer_);

	// Set shader texture resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &texture);
}

void PointsToQuadShader::SetShaderParameters(ID3D11DeviceContext * deviceContext, const XMMATRIX & world_matrix, const XMMATRIX & view_matrix, const XMMATRIX & projection_matrix,
	XMFLOAT4 colour, bool is_offset_by_up) {
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	PointToQuadType* pointToQuadPtr;
	unsigned int bufferNumber;
	XMMATRIX tworld, tview, tproj;

	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(world_matrix);
	tview = XMMatrixTranspose(view_matrix);
	tproj = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	dataPtr = (MatrixBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = tworld;
	dataPtr->view = tview;
	dataPtr->projection = tproj;

	// Unlock the constant buffer.
	deviceContext->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Now set the constant buffer in the geometry shader with the updated values.
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &matrix_buffer_);

	// Additional
	// Pixel Shader, Geometry Shader.
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(point_to_quad_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	// Get a pointer to the data in the constant buffer.
	pointToQuadPtr = (PointToQuadType*)mappedResource.pData;
	pointToQuadPtr->colour = colour;
	pointToQuadPtr->type = XMINT4((int)is_offset_by_up, false, 0, 0);

	// Unlock the constant buffer.
	deviceContext->Unmap(point_to_quad_buffer_, 0);

	bufferNumber = 0;
	// Now set the constant buffer. PS
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &point_to_quad_buffer_);

	bufferNumber = 1;
	// Now set the constant buffer. GS
	deviceContext->GSSetConstantBuffers(bufferNumber, 1, &point_to_quad_buffer_);
}

void PointsToQuadShader::Render(ID3D11DeviceContext* deviceContext, int index_count) {
	// Set the sampler state in the pixel shader.
	deviceContext->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(deviceContext, index_count);
}



