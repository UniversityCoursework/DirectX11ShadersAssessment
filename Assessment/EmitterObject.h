#ifndef _EMITTER_GAMEOBJECT_H
#define _EMITTER_GAMEOBJECT_H

#include "GameObject.h"
#include "ParticleEmitter.h"

/// <summary>
/// Emitter object for controlling and modifing Emitters in the editor.
/// </summary>
class EmitterObject : public GameObject {
public:
	EmitterObject();
	EmitterObject(std::string name);
	~EmitterObject();

	/// <summary>
	/// Whether to use the camera's up vector from the view matrix. Or to use the default (0,1,0) up vector.
	/// </summary>
	/// <param name="offset"> Set True for camera's Up vector to be used in particle quad calculation. </param>
	void SetOffsetByUp(bool offset);

	/// <summary>
	/// Particle Emitter this object represents.
	/// ParticleContainers arent shared like most other meshes, Unless you wish for them to be identical.
	/// </summary>
	/// <param name="particle_emitter"> New particle emitter. </param>
	void SetEmitter(ParticleEmitter* particle_emitter);

	/// <summary>
	/// How many emits per second. (limited by refresh rate etc.)
	/// Not to be conused with how many are released each emit.
	/// </summary>
	/// <param name="rate"> New Rate, (1.0/rate) = timer delay.</param>
	void SetEmissionRate(float rate);


	/// <summary>
	/// Moves the Emitter Position.
	/// Overrides the game objects position.
	/// :/ would be using different technique for a more GPU driven approach. 
	/// As CPU driven, setting current position of emitter probably best.
	/// </summary>	
	void SetPosition(float x, float y, float z) override;


	bool IsOffsetByUp();

	void TurnOn();
	void TurnOff();
	bool IsOn();

	float EmissionRate();
	XMFLOAT3 Position() override;

	/// <summary>
	/// Number of Active Particles.
	/// </summary>
	int NumOfParticles();

	/// <summary>
	/// Maximum number of particles this Emitter can generate.
	/// </summary>	
	int MaxNumOfParticles();

	/// <summary>
	/// The settings for emitted particles.
	/// All their settings, and ranges for spawn.
	/// </summary>
	/// <returns> Pointer to the emitters particle settings. </returns>
	ParticleSettings* EmitterParticleSettings();
private:
	bool is_offset_by_up_;
	ParticleEmitter* particle_emitter_;
};

#endif
