#include "NoiseProcess.h"

NoiseProcess::NoiseProcess()
	:PostProcess() {

	type_ = PostProcessType::kNoise;
	noise_type_ = NoiseProcessType::kNoise1;
	noise_level_ = 0.012f;
}

NoiseProcess::~NoiseProcess() {
}

NoiseProcessType NoiseProcess::NoiseType() {
	return noise_type_;
}

float NoiseProcess::NoiseLevel() {
	return noise_level_;
}

void NoiseProcess::SetNoiseType(NoiseProcessType new_type) {
	noise_type_ = new_type;
}

void NoiseProcess::SetNoiseLevel(float new_level) {
	noise_level_ = new_level;
}
