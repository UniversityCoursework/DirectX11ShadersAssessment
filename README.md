# Graphics Programming Assessment Repo
## DirectX11 Shader Examples
Assessment Application:

3rd Year Graphics Programming for shaders using DirectX11.
 - Multiple Lights with specular/attenuation etc.
 - Tesselation of a sphere, with Nvidia wave function passed through vertices.
 - Post Proccess - Noise, Chromatic aberation.
 - Geometry shader - particle effects from custom point mesh, fire and smoke trail.