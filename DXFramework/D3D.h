// D3D.h
#ifndef _D3D_H_
#define _D3D_H_

// Link libaries dxgi.lib, d3d11.lib, d3dx11.lib, d3dx10.lib

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

class D3D {
public:
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p) {
		_mm_free(p);
	}

	D3D(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, float screenDepth, float screenNear);
	~D3D();

	void BeginScene(float r, float g, float b, float a);
	void EndScene();

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();

	void GetProjectionMatrix(XMMATRIX&);
	void GetWorldMatrix(XMMATRIX&);
	void GetOrthoMatrix(XMMATRIX&);

	void GetVideoCardInfo(char*, int&);

	void TurnZBufferOn();
	void TurnZBufferOff();

	void TurnOnAlphaBlending();

	void TurnOffBlending();

	void SetBackBufferRenderTarget();
	void ResetViewport();

	void TurnOnWireframe();
	void TurnOffWireframe();
	bool IsWireFrameModeOn();

	// Turns back on writing to the depth buffer.
	void TurnZBufferWriteOn();
	// Turns off the ability to write to the depth buffer, still runs the depth test.
	void TurnZBufferWriteOff();

	// Uses additive blending to mix colours together.
	void TurnOnAdditiveBlending();

	// Reset HS, DS, GS shaders, useful for when you no longer wish to use thses shaders.
	// i.e after finished with tesselation
	void ResetShadersNull();

	// True when it attempts to change resolution, ensure valid resolution is passed.
	// hwnd used to set to centre of screen.
	bool ChangeResolution(int screen_width, int screen_height, HWND hwnd);

	bool IsFakeFullScreen();

	// Extension of Change resolution, create the d3d at the max size available for the monitor 
	// without any borders etc. Saves the previous resolution to change back to when turned off.
	// Will only work on Monitor max Resolution.
	// hwnd used to set to centre of screen.
	// Returns true when succesfully set to FullScreenWindowed.		
	bool TurnFullScreenWindowedOn(HWND hwnd);

	// Reverts back to the old resolution setting saved when it went fullscreen.
	bool TurnFullScreenWindowedOff(HWND hwnd);

	// Whether the device is already in fullscreen Exclusive mode.
	bool IsFullscreen();

	// True when set to fullscreen exclusive mode
	bool TurnFullscreenOn();

	// True when attempts to turn off fullscreen exclusive mode
	bool TurnFullscreenOff();

	// The current directX width/height in pixels.
	// Usefull for when in exclsive mode, as screen size will not match pixel resolution.
	XMFLOAT2 Resolution();
protected:

	bool m_is_wireframe_on;
	bool m_vsync_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	ID3D11RenderTargetView* m_renderTargetView;
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilState* m_depthStencilState;
	ID3D11DepthStencilView* m_depthStencilView;
	ID3D11RasterizerState* m_rasterState;
	ID3D11RasterizerState* m_rasterStateWF;
	XMMATRIX m_projectionMatrix;
	XMMATRIX m_worldMatrix;
	XMMATRIX m_orthoMatrix;
	ID3D11DepthStencilState* m_depthDisabledStencilState;
	ID3D11BlendState* m_alphaEnableBlendingState;

	D3D11_VIEWPORT viewport;

	int screen_width_;
	int screen_height_;
	float screen_depth_;
	float screen_near_;
	bool is_fake_fullscreen_;

	int fake_saved_screen_width_;
	int fake_saved_screen_height_;

	ID3D11BlendState* disableBlendingState_;
	ID3D11BlendState* additiveEnableBlendingState_;	
	ID3D11DepthStencilState* depthWriteDisabledStencilState_;

	// Sets up all the Buffers, states and views required for DX11
	// The depth,alpha,stencil and render target.
	void SetBuffers();

	// Releases and nulls all the buffers used by DX11
	void ReleaseBuffers();

	// Resizes the viewport and recalculates all the matrices required for projection.
	void ResizeScreen();
};

#endif