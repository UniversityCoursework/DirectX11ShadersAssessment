// Light class
// Holds data that represents a light source

#include "light.h"

#include <assert.h>

Light::Light() {
	SetAmbientColour(0,0,0,1.0f);
	SetDiffuseColour(0, 0, 0, 1.0f);
	SetDirection(0, 0, 0);
	SetSpecularColour(0, 0, 0, 1.0f);
	SetSpecularPower(2.0f);
	SetPosition(0, 0, 0);
	SetLookAt(0, 0, 0);

	m_lightType = kOff;
	
	m_constant_factor = 1.0f;
	m_linear_factor = 0.125f;
	m_quadratic_factor = 0.0f;
	m_range = 50.0f;
}

void Light::GenerateViewMatrix() {
	
	XMVECTOR look_at_direction = m_lookAt - m_position;

	// Cant have a look and position the same...as you then have no direction -.-
	assert(!XMVector3Equal(look_at_direction, XMVectorZero()));

	XMVECTOR up = XMVectorSet(-XMVectorGetY(look_at_direction), XMVectorGetX(look_at_direction), 0.0f, 1.0f);

	m_viewMatrix = XMMatrixLookAtLH(m_position, m_lookAt, up);
}

void Light::GenerateProjectionMatrix(float screenNear, float screenFar) {
	float fieldOfView, screenAspect;

	// Setup field of view and screen aspect for a square light source.
	fieldOfView = (float)XM_PI / 2.0f;
	screenAspect = 1.0f;

	// Create the projection matrix for the light.
	m_projectionMatrix = XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, screenNear, screenFar);
}

void Light::GenerateOrthoMatrix(float screenWidth, float screenHeight, float near, float far) {
	m_orthoMatrix = XMMatrixOrthographicLH(screenWidth,screenHeight,near,far);
}

void Light::SetType(LightType type) {
	m_lightType = type;
}

void Light::SetAmbientColour(float red, float green, float blue, float alpha) {
	m_ambientColour = XMFLOAT4(red, green, blue, alpha);
}

void Light::SetDiffuseColour(float red, float green, float blue, float alpha) {
	m_diffuseColour = XMFLOAT4(red, green, blue, alpha);
}


void Light::SetDirection(float x, float y, float z) {
	m_direction = XMFLOAT3(x, y, z);
}

void Light::SetSpecularColour(float red, float green, float blue, float alpha) {
	m_specularColour = XMFLOAT4(red, green, blue, alpha);
}


void Light::SetSpecularPower(float power) {
	m_specularPower = power;
}

void Light::SetConstantFactor(float constant) {
	m_constant_factor = constant;
}

void Light::SetLinearFactor(float linear) {
	m_linear_factor = linear;
}

void Light::SetQuadraticFactor(float quadratic) {
	m_quadratic_factor = quadratic;
}

void Light::SetRange(float range) {
	m_range = range;
}

void Light::SetPosition(float x, float y, float z) {
	m_position = XMVectorSet(x, y, z, 1.0f);
}

void Light::SetLookAt(float x, float y, float z) {
	m_lookAt = XMVectorSet(x, y, z, 1.0f);
}

LightType Light::GetType() {
	return m_lightType;
}

XMFLOAT4 Light::GetAmbientColour() {
	return m_ambientColour;
}

XMFLOAT4 Light::GetDiffuseColour() {
	return m_diffuseColour;
}


XMFLOAT3 Light::GetDirection() {
	return m_direction;
}

XMFLOAT4 Light::GetSpecularColour() {
	return m_specularColour;
}


float Light::GetSpecularPower() {
	return m_specularPower;
}

XMFLOAT3 Light::GetPosition() {
	XMFLOAT3 temp(XMVectorGetX(m_position), XMVectorGetY(m_position), XMVectorGetZ(m_position));
	return temp;
}

XMFLOAT3 Light::GetLookAt() {
	XMFLOAT3 temp(XMVectorGetX(m_lookAt), XMVectorGetY(m_lookAt), XMVectorGetZ(m_lookAt));
	return temp;
}


XMMATRIX Light::GetViewMatrix() {
	return m_viewMatrix;
}

XMMATRIX Light::GetProjectionMatrix() {
	return m_projectionMatrix;
}

XMMATRIX Light::GetOrthoMatrix() {
	return m_orthoMatrix;
}

float Light::GetConstantFactor() {
	return m_constant_factor;
}

float Light::GetLinearFactor() {
	return m_linear_factor;
}

float Light::GetQuadraticFactor() {
	return m_quadratic_factor;
}

float Light::GetRange() {
	return m_range;
}
